def max_fun(tree):
    """
    returns the maximum amount of fun that a party can have assuming that
    a party cannot have any two employees such that the two employees are
    directly connected in the hierarchy (described in tree) in a child-parent or
    parent-child relationship
    """
    ordered_nodes = BFS(tree) # assumes root is in 0th position
    ordered_nodes = enumerate(ordered_nodes) # pairs each node with its index
    ordered_nodes.reverse()

    node_to_num = {node: num for num, node in ordered_nodes}

    dp_table = [0] * len(ordered_nodes) # initialize the table to n zeros

    for index, node in ordered_nodes:
        without_me = 0
        for child in node.children:
            without_me += dp_table[node_to_num[child]]

        with_me = node.fun_coef
        for child in node.children:
            if not child.children:
                break

            for gchild in child.children:
                with_me += dp_table[node_to_num[gchild]]

        dp_table[index] = max(without_me, with_me)

    return dp_table[0]







