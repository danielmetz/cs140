\documentclass[11pt]{article}
\usepackage[nostamp]{../cs140}

\begin{document}

\begin{header}
\psheader{3}{70}{1:15PM}{Monday, September 14 + Wednesday, September 16}
\end{header}

\begin{itemize}
\item This problem set should be done in pairs.
\item You must use \LaTeX $\;$ to format (the non-code portion of) your solution.  
\end{itemize}

\begin{enumerate}


\item \problem{10}{Solving Recurrences} 

Give asymptotic upper and lower bounds for $T(n)$ in each of the
following recurrences.  Assume that $T(n)$ is constant for $n \leq 2$.
Make your bounds as tight as possible (in other words, give $\Theta$ bounds
where possible) and make sure to justify your answers.  
(Note: 
you may want to read appendix A.1 in the
textbook to remind yourself of basic summation properties.)

\begin{enumerate}
\item $T(n) = 4T(n/2) + cn$
\item $T(n) = T(n-1) + n$
\item $T(n) = T(n-1) + 1/n$
\item $T(n) = T(9n/10) + n$
\item $T(n) = 2T(n/4) + \sqrt{n}$
\end{enumerate}

\vspace*{.2cm}

\item \problem{20}{3-part sort}

Consider the following sorting algorithm: First sort the first
two-thirds of the elements in the array. Next sort the last two thirds
of the array.  Finally, sort the first two thirds again.  Notice that
this algorithm does not allocate any extra memory; all the sorting is
done inside array $A$.  Here's the code: 

\begin{tabbing}
\hspace*{1cm} \= ThreeSort ($A$,$i$,$j$) \\
\> \hspace*{.5cm} \= {\bf if} \= $A[i] > A[j]$ \\
\>\>\> {\bf swap} $A[i]$ and $A[j]$ \\
\>\> {\bf if} $i+1 \geq j$ \\
\>\>\> {\bf return} \\
\>\> $k = \lfloor (j-i+1)/3 \rfloor$. \\ 
\>\> ThreeSort$(A,i,j-k)$ \ \ \ \ \ \= {\bf Comment:} Sort first two-thirds. \\
\>\> ThreeSort$(A,i+k,j)$ \> {\bf Comment:} Sort last two thirds.\\            
\>\> ThreeSort$(A,i,j-k)$ \> {\bf Comment:} Sort first two-thirds again! \\     
\end{tabbing}

\begin{enumerate}
\item  Give an informal but convincing explanation (not a rigorous proof 
by induction) of why the 
		approach of sorting the first two-thirds of the array, 
then sorting the last two-thirds of the array,
		and then sorting again the first two-thirds of the 
array yields a sorted array.  A few well-chosen 				sentences should suffice here. 

\item Find a recurrence relation for the worst-case running time of
ThreeSort.

\item Next, solve the recurrence relation using a recursion tree.

\item Double check that you got the right answer in the previous part
by solving the recurrence using the master theorem.

\item How does the worst-case running time of ThreeSort compare with
the worst-case running times of Insertion Sort, Selection Sort, and 
Mergesort?
\end{enumerate}

\item \problem{40}{Stock Market Problem (with code!) --- this part is due on Wednesday, September 16}

\begin{quote}
    The difference between art and science is that science is what we
    understand well enough to explain to a computer. Art is everything
    else. -- Don Knuth
\end{quote}

Recall that you're working at a brokerage firm which periodically
examines how a particular stocok has done in the last $n$ days.  This
time you need to compute the longest consecutive number of days in 
which the stock's value did not decrease.  For example, consider the 
stock values below:
\begin{verbatim}
Day:      1   2   3   4   5   6   7   8
Value:    42  40  45  45  44  43  50  49
\end{verbatim}
In this example, the length of the longest consecutive non-decreasing 
run is 3.  This run goes from day 2 to day 4.  

Here are your tasks:
\begin{enumerate}
\item Briefly describe 
  a very simple ``naive'' algorithm for this problem and explain
  why the worst-case running time is $\Theta(n^2)$.
\item Describe a divide-and-conquer algorithm whose running time is
  asymptotically better than $\Theta(n^{2})$.  Provide pseudocode
  and/or a clear English description of your algorithm.  (Note that
  your algorithm must be a divide-and-conquer algorithm.)  
\item Analyze the running time of your algorithm.
\item Implement your divide-and-conquer algorithm in one of
  the following languages: C, C++, Java, or Python (ask an instructor first
  if you want to use another language).   Your code
  must allow the user to specify two filenames, say {\tt infile.txt}
  and {\tt outfile.txt} on the command line (we don't want to edit
  your code).  For example, if using C, your code might be
  executed as follows:
  \begin{verbatim}
      % ./stockmarket infile.txt outfile.txt
  \end{verbatim}
  The contents of the input file will be in the
  following form:
  \begin{verbatim}
    8
    42
    40
    45
    45
    44
    43
    50
    49
\end{verbatim}
The first line specifies the number of days (which may not be a power of 2!).  The
following lines give the value of the stock on each day.  

In this case the output file should contain the following:
\begin{verbatim}
    3
    2
    40
    45
    45
\end{verbatim}
The first line gives the length of the longest non-decreasing subsequence,
the second line gives the day on which the subsequence begins (note the
1-based indexing!), and the rest of the lines give the prices of the
stock on the days included in the subsequence.

On the written (in \LaTeX)
problem set that you turn in on Tuesday, the solution to this part of
the problem should consist of the following information:
\begin{itemize}
\item where to find your well-commented code (ie, a directory that I can 
  access, a web page, dropbox, etc).
\item how to compile and run your code (be very, very precise and keep in
  mind that it will be tested on {\tt project2.cs.pomona.edu})
\end{itemize}

Most of the grade will be based on correctly implementing the algorithm
you described in part 2 (so please make it easy for us to tell what
your code is doing) and on whether the code computes the right answer
for our test files (as determined by {\tt diff}, so make sure your output 
is in the format described above).  We may also
consider code efficiency in an absolute (wall clock) sense.  

(Small) sample input and output files will be on the course
webpage.  Note that if there are two subsequences of equal length,
either is an acceptable answer.

\end{enumerate}


\item \problemBonus{Chip Testing}

You have $n$ supposedly identical computer chips that in principle are capable of testing each other. Your test jig accommodates two chips at a time. When the jig is loaded, each chip tests the other and reports whether it is good or bad. A good chip always reports accurately whether the other chip is good or bad, but the answer of a bad chip cannot be trusted. Thus, the four possible outcomes of a test are as follows:
\begin{verbatim}
Chip A says        Chip B says         Conclusion
------------       -------------       ------------

B is good          A is good           Both good, or both bad
B is good          A is bad            At least one is bad
B is bad           A is good           At least one is bad
B is bad           A is bad            At least one is bad
\end{verbatim}

\begin{enumerate}
	\item Show that if more than $\frac{n}{2}$ chips are bad, you cannot necessarily determine which chips are good using any strategy based on this kind of pairwise test. Assume that the bad chips can conspire to fool you.
	\item Consider the problem of finding a single good chip from among n chips, assuming that more than $\frac{n}{2}$ of the chips are good. Show that $\lfloor \frac{n}{2} \rfloor$ pairwise tests are sufficient to reduce the problem to one of nearly half the size.
	\item Show that the good chips can be identified with $\Theta(n)$ pairwise tests, assuming that more than $\frac{n}{2}$ of the chips are good.  Explain clearly why your algorithm identifies all of the good chips.  Then give and solve the recurrence that describes the number of tests.
\end{enumerate}

\end{enumerate}

\end{document}