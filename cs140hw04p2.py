def index_problem(arr, low=None, high=None):
    """
    NOTE: we assume 0-indexing.

    This algorithm checks recursively if there exists some i in range(len(arr))
    for which i == arr[i].

    At each iteration, this algorithm will compare the midpoint against the
    value at the midpoint, concluding either that mid == arr[mid], or inducing
    recursion on the appropriate half of arr (determined by mid < arr[mid]). We
    know this to be valid, as we may assume the input array to be sorted in
    ascending order, and therefore if mid < arr[mid], i < arr[i] for all i >
    mid. As a result, if a solution exists it will exist in the half preceding
    mid. We give up when we've reduced our search scope to a single element, and
    that element still fails to satisfy our condition of interest.

    inputs:
        arr (int array): an ascending, sorted array of ints
        low (int): smallest index to be considered in the array
            gets set to zero if not provided; may be changed in recursion
        high (int): highest index to be considered in the array
            gets set to len(arr)-1 if not provided; may be changed in recursion
    """
    # set low and high to the indices at each end if not provided
    low = 0 if low is None else low
    high = len(arr)-1 if high is None else high

    # calculate the midpoint
    mid = (low + high) / 2

    # case where found a case where i = A[i]
    if mid == arr[mid]:
        return True

    # base case where we've narrowed our search to a single
    # element that fails to satisfy i = A[i]
    elif low == high:
        return False

    # case where we recurse on left half
    elif mid < arr[mid]:
        return index_problem(arr, low, mid)

    # case where we recurse on right half
    else:
        return index_problem(arr, mid, high)