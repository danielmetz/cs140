\newcommand{\balg}{Bor\.{u}vka's Algorithm\xspace}

\begin{enumerate}[(a)]
\item Consider a graph composed of three vertices and an edge of weight one
  connecting all pairs of vertices. Under this algorithm, it is possible that
  all three edges are selected to form our ``MST'' when in truth we only require
  two edges (and can therefore from a different tree which spans and has lower
  total weight).
\item Note that the edges selected by \balg at each moment can be thought of as
  a light edge that crosses some cut that respects connected components. We
  argue this as a) edges must contain one vertex in C and one not in C
  (therefore the cut we're considering must respect our set of connected
  components) and b) the edge is necessarily a light edge as we're choosing the
  least-weight edge for each connected component. So far then we have that each
  edge being selected, at the time of its selection, is a safe edge for
  \emph{some} minimum spanning tree. If all our edge weights are distinct, there
  will exists only one minimum spanning tree, therefore making each edge
  selection safe for the \emph{same}. Note then that because our edge selection
  abides by our theorem regarding MSTs (Theorem 23.1 in Cormen), we conclude
  that the tree we build out will indeed be an MST by the time it has \(n-1\)
  edges.
\item If some edges in the graph have the same edge-weight, there may exist more
  than one MST (i.e. there does not exist a singularly unique MST). As a result,
  our edge-selection doesn't necessarily guarantee that the edges being selected
  all belong to the \emph{same} MST and therefore their aggregate isn't promised
  to form a MST. We require all edges to have unique weights so as to ensure
  that there exists only one MST.
\item We could modify \balg such that when considering edges leaving a
  particular connected component, if the least-weight edge is non-unique, we
  check to see if one of these edges has already been ``chosen.'' If so, the
  modified algorithm must select the previously chosen edge.
\item Consider our input graph in the form of adjacency lists and consider that
  we have some function \(w(v_1, v_2)\) that identifies the weight of an edge
  from \(v_1\) to \(v_2\) (or notes that no such edge exists). Consider this
  function to be implemented as a hash-table hookup, and therefore calls to this
  function may be completed in \bigo{1} time. We choose to represent connected
  components using disjoint set forests. Our algorithm will work roughly as
  described in \Cref{alg:helpers} and \Cref{alg:balg-code}.

  To add a prosaic explanation, our proposed algorithm will work as follows: It
  begins by initializing a empty set to ultimately contain those edges which
  will be a part of our MST. We then proceed to call \texttt{makeSet} on
  each vertex. Next, we start a while loop, contingent upon whether or not all
  vertices together are part of just one connected component. This check, to see
  if all vertices belong to the same connected component, is implemented by
  calling \texttt{findSet} on each vertex and returning whether or not the count
  of unique ``representatives'' is one (therefore all is connected), or
  greater. Inside the while loop, we'll iterate through each connected
  component. For each vertex that's part of a connected component, we'll look at
  all its edges. For each edge that has it's other end in a different connected
  component, we'll add it to a set of possible edge choices. Once we're done
  iterating through the vertices, we'll find the minimum weight edge(s). If any
  have already been chosen, we proceed to the next iteration of the connected
  component loop. Otherwise, we'll choose the first of the edges of minimum
  weight. After looping through each connected component, we'll add all those
  edges which we've chosen. We then repeat our while loop, recalculating
  connected components, etc.

  Note that \texttt{is\_connected} runs in approximately \bigo{\abs{V}}
  amortized time as \texttt{find\_set} is approximately amortized \bigo{1}
  operation (due to union-by-rank and path compression). We acknowledge that
  strictly it is super-linear, but we choose to hand-wave away this
  acknowledgment by reference to the textbook argument on p572 (in short:
  inverse Ackerman).

  Walking through \texttt{b\_alg} then, let's check our run-time. the first
  for-loop runs in \bigtheta{\abs{V}} time. Our while loop is where things get
  interesting. The \texttt{for CC in CCs} loop iterates through all \abs{V}
  vertices in that CC (and no vertex belongs to two CCs, so across all runs of
  this out \texttt{for CC in CCs} loop, the \texttt{for vertex in CC} loop
  iterates through all vertices once). Inside that vertex loop, we consider all
  edges containing that vertex. Thus altogether, the entire \texttt{for CC} loop
  runs in \bigtheta{\abs{E}} time. To calculate the min\_weight will be less
  than this, as will be to choose from amongst our candidates. We can fold-up
  then that the work done inside the \texttt{for CC} loop to be
  \bigtheta{\abs{E}}. After that loop, we iterate through all edges, checking
  for which have been chosen, and unioning where appropriate (a \bigo{1} step).
  Together then this remainder of the while loop then takes \bigtheta{\abs{E}}
  time too. The question then is how many times will the \texttt{while} loop
  run? In the worst-case, each connected component unions itself with exactly
  one other connected component (the case when that other connected component
  also chooses an edge which unifies with the first). This halves our number of
  connected components after each iteration, which we can only do \(\log
  \abs{V}\) times.

  Altogether then we have that our algorithm runs in worst-case
  \bigtheta{\abs{E} \log \abs{V}} time.

\begin{figure}[H]
\caption{Helper Functions}
\label{alg:helpers}
\begin{minted}{python}
def is_connected(V):
  '''returns true if all vertices are part of the same connected component'''

  # initialize a set to contain the set reps
  set_reps = Set()

  # add the set rep for each vertex to our set of set reps
  for vertex in V:
    set_reps.add(find_set(vertex))

  # if there was only one set rep, then everything's connected
  return len(set_reps) == 1:

def get_CCs(V):
  '''
  creates a dictionary of set_reps to set elements
  in effect creating a way to iterate through connected components
    (and their respective elements)
  '''
  CCs = defaultdict(set)
  for vertex in V:
    CCs[find_set(vertex)].add(vertex)

  return CCs
\end{minted}
\end{figure}

\begin{figure}[H]
\caption{Our \balg ``Implementation''}
\label{alg:balg-code}
\begin{minted}{python}
def b_alg(V, E_list, E_pairs, W):
  '''
  V is our set of vertices
  E_list maps a vertex to the vertices to which it has an edge
  E_pairs is a set of tuples like (v_1, v_2)
  W is the weight function for edges
  '''
  final_edges = {} # initialize empty set

  # make each vertex its own connected component
  # (utilizing disjoint set forests)
  for vertex in V:
    make_set(vertex)

  # so long as we haven't spanned...
  while not is_connected(V):
    CCs = get_CCs(v)
    # for each connected component
    for CC in CCs:
      edge_choices = {} # initialize empty set
      # for each vertex inside this connected component
      for vertex in CC:
        # for each edge from vertex outward
        for end_vertex in E_list[vertex]:
          # add the edge if it extends the connected component
          if not end_vertex in CC
            edge_choices.add((vertex, end_vertex))

      # now that we're done adding possible edges, lets find the lightest one(s)
      min_weight = min(W(edge) for edge in edge_choices)
      candidates = {edge for edge in edge_choices if W(edge) == min_weight}

      # if no edge has already been marked chosen, choose the first
      if not any(edge.chosen for edge in candidates):
        candidates[0].chosen = True

    # add the edges we selected before
    # adjust connected components
    # reset chosen bool
    for edge in E_pairs:
      if edge.chosen:
        edge[0].union(edge[1])
        final_edges.add(edge)
    for edge in E_pairs:
      edge.chosen = False

  return final_edges
\end{minted}
\end{figure}
\end{enumerate}