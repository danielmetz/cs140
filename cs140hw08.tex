\documentclass{article}
\usepackage[vmargin=.75in]{geometry}
\usepackage{fancyhdr}
\usepackage[shortlabels]{enumitem}
\usepackage{booktabs}
\usepackage{cleveref}
\usepackage{commath}
\usepackage{qtree}
\usepackage{subcaption,float}

\newcommand{\bigo}[1]{\ensuremath{\mathcal{O}(#1)}}
\newcommand{\usd}[1]{\$#1}

\pagestyle{fancy}
\lhead{CS 140}
\chead{Assignment 8}
\rhead{Daniel Metz \& Huu Le}
\cfoot{\thepage}

\begin{document}
\section{Disjoint Sets}
\begin{enumerate}[(a)]
\item To imagine the resulting data structures, consider \cref{tab:1a1} and
\cref{tab:1a2}. The ``value'' row corresponds to the linked list in structure, where
the value of the first element of each linked list respectively is 1 and 13,
(which respectively contain pointers to the next elements in their linked lists
2 and 14, etc). The ``set rep'' row for each table currently shows what the
representative will be for that linked list. However, this isn't quite right.
What it should really be is that all values in the set rep row should be
considered to point to the set object, which in turn includes a pointer to both
the head and the tail.

The answers returned by the two calls to \texttt{FindSet} are both 1.

\begin{table}[H] \centering \hspace*{-4em}
    \begin{tabular}{llllllllllllllllllllllll} \toprule
    set rep & 1 & ~ & 1 & ~ & 1 & ~ & 1 & ~ & 1 & ~ & 1 & ~ & 1 & ~ & 1 & ~ & 1 & ~
    & 1 & ~ & 1 & ~ & 1 \\ \midrule
    value & 1 & \(\to\) & 2 & \(\to\) & 3 & \(\to\) & 4 & \(\to\) & 5 & \(\to\) &
    6 & \(\to\) & 7 & \(\to\) & 8 & \(\to\) & 9 & \(\to\) & 10 & \(\to\) &
    11 & \(\to\) & 12 \\ \bottomrule
    \end{tabular}
    \caption{First Table for 1 (a)}
    \label{tab:1a1}
\end{table}

\begin{table}[H] \centering
    \begin{tabular}{llllllllllllllllllllllll} \toprule
    set rep & 13 & ~ & 13 & ~ & 13 & ~ & 13 & \\ \midrule
    value & 13 & \(\to\) & 14 & \(\to\) & 15 & \(\to\) & 16 \\ \bottomrule
    \end{tabular}
    \caption{Second Table for 1 (a)}
    \label{tab:1a2}
\end{table}

\item The data structure that results from the completion of the above code is
captured in \cref{fig:tree1ba} and \cref{fig:tree1b2}. Well, almost\ldots.
Consider the images amended such that every edge connecting parent and child
node is a directed pointer from the child to the parent. Furthermore, all root
nodes (i.e. those without parents) point to themselves.

Finally, the answers returned by the two calls to \texttt{FindSet} are the same
as in (a): both return the representative 1.

\begin{figure}[H] \centering
    \begin{subfigure}[b]{.45\textwidth}
        \Tree [.1
                    [.2 ]
                    [.3 [.4 ] ]
                    [.5 [.6 ]
                            [.7 [.8 ] ]
                    ]
                    [.9 [.{10} ]
                        [.{11} [.{12} ] ]
                    ]
              ]
        \caption{First Tree for 1 (b)}
        \label{fig:tree1ba}
    \end{subfigure}
    \(\quad\)
    \begin{subfigure}[b]{.45\textwidth}
        \Tree [.{13}
                    [.{14} ]
                    [.{15} [.{16} ]
                    ]
              ]
        \caption{Second Tree for 1 (b)}
        \label{fig:tree1b2}
    \end{subfigure}
\end{figure}


\newpage
\section{Lazy Binomial Heaps}

\begin{enumerate}[(a)]
\item % 2 (a)
    \begin{description}
    \item[NEW-HEAP] This function takes no inputs. It merely has to make a new,
        empty heap, which itself needs only to contain a null pointer and a size
        counter (set to zero). All these operations are \bigo{1} operations,
        thus the entire function is \bigo{1}.
    \item[INSERT] This function needs only to construct a new binomial tree of
        type 0 (a \bigo{1}) operation. It then inserts arbitrarily in the lazy
        binomial heap, either at the beginning or end of its linked list (again
        a \bigo{1} operation). It must determine whether it is a new min (again,
        \bigo{1} time), and lastly must increment the size of the heap (still,
        \bigo{1} time). Since both these steps are \bigo {1}, the entire
        function call is as well.
    \item[FIND-MIN] We already keep a pointer to the smallest element, so we
        need only follow this pointer and return the corresponding element.
        \bigo{1} time easily!
    \item[UNION] This function merely attaches one heap to another in the
        easiest of ways, compares their minimums to update the minimum pointer,
        and calculates the merged size of the result. All of these are \bigo{1}
        operations, so in sum they are too as there is a constant number of
        these operations.
    \end{description}

    \textsc{Note}: for all of the above descriptions, it's only ever a constant
    number of \bigo{1} operations that are summed together. Because of this
    independence of any \(n\) (in both any individual costs as well as any
    number of calls to cheap operations), these sums then are each \bigo {1}.
\item % 2 (b)
    \begin{description}
    \item[Base Case] Observe that this is trivially true for the binomial tree
    of rank 0 (root-only tree) as indeed this tree of one node has \(1 =
    2^0\).
    \item[Inductive Hypothesis] Assume this holds for a binomial tree of rank
    \(k\) (that a binomial tree of rank \(k\) has \(2^k\) nodes).
    \item[Inductive Step] Furthermore, recall as described in class that a
    binomial tree of rank \(k\) is composed of two binomial trees of rank
    \(k-1\). It follows then a binomial tree of rank \(k\) has twice the number
    of nodes as a binomial tree of rank \(k-1\). Since the binomial tree of rank
    \(k-1\) had a number of nodes that was a power of two, doubling this node
    count will too result in a power of two.
    \end{description}
    Thus we conclude that a binomial tree of rank \(k\) has \(2^k\) nodes.
\item % 2 (c)
    Recall that as mentioned in class, a binomial tree of rank \(k\) can be
    decomposed into a root node with children that are themselves binomial trees
    of ranks 0 through \(k-1\). Each of these child binomial trees have a number
    of nodes that are powers of 2 (by our analysis of binomial trees in (b)),
    and by removing the root, the minimum element, its only these child binomial
    trees that remain.
\item % 2 (d)
    Suppose that each of the \(\ell\) trees have \usd{4} to their name. For each
    these trees, we pay \usd{1} to check for emptiness in the appropriate bin of
    the array. If the bin was empty, we then pay \usd{1} to place it into the
    bin. This leaves it with \usd{2}. We use this to be our invariant: that any
    tree in this array must have at least \usd{2}. If the bin was not empty,
    then pay \usd {1} to merge this tree with the tree that was previously
    there. This leaves this combined tree with at least \usd{4}. We then pay
    \usd{1} to peek into the next bin. If this bin is empty, we pay \usd{1} to
    put this merged tree there, leaving that tree with \usd{2}. If the bin was
    not empty, then there should be a tree there that also has \usd{2}. We then
    merge the two trees, leaving this merged tree with \usd{4} and repeat this
    process. Since we do this by requiring only a constant number of dollars to
    be assigned to each of the \(\ell\) trees, we conclude that this is a \bigo
    {\ell} step.
\item % 2 (e)
    After the clean-up phase, we have no duplicate sized binomial trees in our
    lazy binomial heap. Furthermore, we know all binomial trees to have sizes
    equal to powers of two. Since we know (via binary representation) that any
    positive integer can be represented as the sum of unique powers of 2 (and
    this requires no more than \(\log n\) bits) we know that a lazy binomial
    heap (post-cleanup) with \(n\) nodes cannot be composed of more than \(\log
    n\) binomial trees.
\item % 2 (f)
    Let our potential function equal the number of binomial trees in our lazy
    binomial heap. Consider the following explorations as we step through
    DELETE-MIN called on a lazy binomial heap composed of \(n\) elements.
    \begin{enumerate}[(a)]
    \item To find the minimum element, to remove it, and to decrement our
        size is all \bigo{1} work. This also means this is \bigo{\log n} work
        since \(\bigo{1} \in \bigo{\log n}\).
    \item To add \(k\) resultant trees requires \(k\) work. This value of \(k\)
        is bounded above by \(\log n\) as in the worst-case, the entire heap is
        composed of one large binomial tree, which when its minimum is removed,
        leaves \(\log n\) binomial trees in its place. Thus \bigo{\log n} for
        this step too.
    \item In the worst-case, every tree must be involved in a merge, and the
        resultant merged trees must be involved in merges, until there is left
        only one tree. Since merges involve trees of equal size, the number of
        possible merges is bounded by \(\log n\) as this is the maximum number
        of times we can merge things of equal size to end up with a single thing
        of size \(n\). Again \bigo{\log n} for this step.
    \item To string the trees together walking through the array (of length
        \(\log n\)) is a \bigo{1} operation for each append to the linked-list,
        and thus bounded above by \(\log n\) as there are only \(\log n\) bins
        in our array. Per usual at this point, \bigo{\log n} for this step too.
    \item Lastly, to find our new minimum requires only a single traversal
        through our linked list of binomial trees. Since each tree is a heap
        with its minimum element as its root, and there are at most \(\log n\)
        of these trees, the work in this step too is within \bigo{\log n}.
    \end{enumerate}
    Because each of the above steps is involves at most some amount of work
    contained in \bigo{\log n} and there is only a constant number of steps, the
    total work for the above is also \bigo{\log n}. The final number of binomial
    trees in our lazy binomial heap is bounded above by \(\log n\) (think
    binary representation). The initial number of binomial trees in our lazy
    binomial heap is bounded above by \(n\) (think all trees of size one).
    As a result, our final evaluation of cost plus end result of the potential
    function minus the initial result of the potential function is then bounded
    between \(c \log n + \log n - n \) and \(c \log n + 1 - n\). Because this
    \(n\) is being subtracted, this sum of the actual work done and the
    difference in the potential function is in \bigo{\log n} (as it can be
    bounded above by some constant times \(\log n\)). Thus we have shown
    DELETE-MIN to be amortized \bigo{\log n}.
\item % 2 (g)
    \begin{description}
    \item[NEW-HEAP] This has an actual cost on the order of \bigo{1} (for
        reasons previously described). There is no change here in the potential
        function. Together then, the sum of the actual costs and the difference
        in potential function is \bigo{1}.
    \item[INSERT] Again, for reasons previously described, the actual costs are
        on the order of \bigo{1}. The change in the potential function is +1 as
        we're adding a new binomial tree (of size 1) to our linked list of
        binomial trees. Together, the sum of the actual costs and the change in
        the potential function are \bigo{1}.
    \item[FIND-MIN] Surprise, the actual costs are \bigo{1} here. The change in
        the potential function is 0. Together these two are \bigo{1}.
    \item[UNION] Finally, the actual costs are \bigo{1} again. The change in the
        potential function is -1. Together these two are \bigo{1}.
    \end{description}
\item % 2 (h)
    We needed to show that the \emph{amortized costs} of the other operations
    was still \bigo{1} for the reason that they were not previously evaluated
    under this particular potential function. It would be theoretically possible
    for a potential function to exist making DELETE-MIN appear to be ``cheap''
    in its amortized cost, but to pull this off in such a way that it would
    manage to make other operations more expensive. We show in (g) that this is
    not the case for our particular functions combined with our chosen potential
    function, and having shown this, may rest happy knowing these operations
    still to be fast.
\end{enumerate}

\end{enumerate}
\end{document}