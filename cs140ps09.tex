\documentclass[11pt]{article}
\usepackage[nostamp]{../cs140}

\begin{document}

\begin{header}
\psheader{9}{120}{1:15PM}{Wednesday, October 21 + Monday, October 26}
\end{header}

Problems 1, 2, and 3 are due on October 21st.  Problem 4 is due on
October 26th.  You may {\bf not} take a 1-day extension on Problem 4,
although you may take a 1-day extension on the 3 problems due 10/21.  
This is because I will be handing out solutions in class on October 26 
so that you can use them to study for the checkpoint on October 28.

\vspace*{.1cm}

You may work either alone or in pairs.  If you would like to work with
a partner you are free to choose that partner.  Note that you may choose
to work with a partner for Part I (problems 1, 2, and 3) and Part 2 
separately.

\vspace*{.2cm}

\begin{enumerate}

\item \textbf{[20 Points] Greedy Course Scheduling}

In class we talked about the problem of finding the maximum number of
non-conflicting courses from a given set of courses.  In fact, if the
courses are sorted in order of completion time, then the following
greedy algorithm is guaranteed to find the maximum number of
non-conflicting courses: Choose the course with earliest completion
time.  Cancel out all courses that conflict with that course.  Now
repeat the process for the remaining courses.  \\ 

Of course, this doesn't mean other greedy algorithms wouldn't also
work to solve this problem.  Consider the following four
alternatives:

\begin{description}
\item[Algorithm 1:]  
Sort the courses by {\em ending} time as before.  Now, run our original greedy
algorithm in the opposite direction.  That is, choose the course that
ends {\em latest}.  Then cancel out all courses that conflict with
that course.  Now repeat the process for the remaining courses.

\item[Algorithm 2:]  
Sort the courses by increasing starting time (rather than ending
time).  Now, choose the course that starts first.  Then cancel out all
courses that conflict with that course.  Now repeat the process for
the remaining courses.

\item[Algorithm 3:]  
Forget about sorting the courses. Choose a course of shortest duration
(that is the course that has the least length).  Then cancel out all
courses that conflict with that course.  Now repeat the process for
the remaining courses.

\item[Algorithm 4:]  
Don't sort the courses.  Choose a course that conflicts with the
fewest other courses, \emph{breaking ties arbitrarily.} Then cancel out the
courses that conflict with that course.  Now repeat the process for
the remaining courses.

\end{description}

In this problem you will explore various aspects of greed in the
context of this problem.

\begin{enumerate}
\item (16 points)
Show that every alternative algorithm is incorrect by giving a
counterexample for each. You should explain your counterexample in
pictorial form. Also, indicate in this picture what the greedy
algorithm could choose and contrast this with a better solution.

\item (4 points) Consider a registrar algorithm that first sorts on {\em start}
time and then uses the following greedy choice: choose the course with
the {\em latest} start time (remove conflicts and repeat).  Argue
that this works.  

So that we can provide useful feedback, don't take a reasonable
``shortcut'' here by observing that since this algorithm is isomorphic 
to the one we proved made a safe choice in class, its choice is also
safe.
\end{enumerate}

\item \textbf{[30 Points] Ski Optimization}

You work at a ski rental place and you would like to design an
algorithm that matches skiers to skis.  Ideally every skier should get
a pair of skis whose length matches his or her height.  Unfortunately,
in general this is not possible.  So, to measure how good a particular
match of ski to skier is, we'll use the {\em disparity} between them,
which we'll define as the absolute value of the difference between the
length of the skis and the height of the skier.  Now we want a way to
assign skis to skiers that minimizes the sum of the disparities.


The input to this problem is an array of $n$ skiers (each skier is a 
pair comprising their name and their height) and an array of $m \geq n$ 
pairs of skis (each ski pair is just the height of the skis).  These 
arrays are given in sorted order from shortest to tallest.


\begin{enumerate}
\item (2 points)
Since the first goal is to have {\em an} algorithm, you start simple.
Describe a brute-force algorithm and explain how long it would take to
execute this algorithm on a computer that performs 1 billion
operations per second if there were 20 skiers and 20 pairs of skis.

\item (2 points) After deciding that you can't wait that long, you consider the
following greedy ``algorithm'': since the skiers and skis are already sorted, 
consider the skiers one-by-one from shortest to
tallest. For each skier under consideration, assign that skier the pair of
skis that most closely match that skier's height. Then remove that skier
and pair of skis from consideration and repeat the process.
Find a small example where this gives a solution
that is worse than optimal.  You'll need to provide a particular small set
of skier heights, a small set of ski heights, show the solution produced
by the greedy ``algorithm'', and then show a solution that is better.

\item (4 points) In search of something better, you realize that if there is a
short person and a tall person, it is never better to give the shorter
person a taller pair of skis than were given to the tall person.
In other words, there exists an optimal solution to the
ski problem in which there are no ``inversions'' in which persons x and y,
with x shorter than y, are assigned skis such that x has longer skis than
y. It turns out that this conjecture is not hard to prove, particularly if
you perform a detailed case analysis. Enumerate the cases that you would
consider in proving this conjecture and then give a short proof of any one
of the cases. (A few sentences should suffice for the proof.)

\item (4 points) Assume there are $n$ people and $n$ pairs of skis.  Describe
(in English sentences) a fast algorithm for assigning skis to 
skiers, briefly explain how the proof of correctness would work, and
give the running time of your algorithm.

\item (18 points) Finally, consider the general case that $m \geq n$.
  \begin{enumerate}
  \item In simple and clear pseudo-code or English, describe a recursive 
    algorithm for solving this problem.  For now, assume that ``solving'' 
    means just finding the number which is the sum of the disparities in an 
    optimal solution (that is, the sum of the differences between the skiers 
    and their skis in an optimal solution).
    Make sure to describe the base cases and the recursive call(s).
  \item Next, describe how you would implement this algorithm using dynamic 
    programming.  In particular, describe what the DP table looks like and 
    the order in which the cells would be filled in.
  \item What is the running time and space of your algorithm? 
  \item Briefly, describe how you could reconstruct an actual optimal 
    solution matching skiers with skis using your DP table.
  \end{enumerate}
\end{enumerate}

\item \textbf{[30  Points] Pretty printing}

  Assume that you are designing a typesetting system which formats text 
  input so that the text is left-justified (all lines begin at the same 
  left column) and the right margin is as even as possible (where ``even
  as possible'' is defined below).

For example, consider the following input:
\begin{verbatim}
Call me Ishmael.  Some
years ago, never mind how long precisely,
having little
or
no money in my purse, and nothing
particular to interest me on shore, I thought I 
would sail
about a little and see the watery part of the
world.
\end{verbatim}
The system should process this input and output something like this:
\begin{verbatim}
Call me Ishmael.  Some years ago, never
mind how long precisely, having little
or no money in my purse, and nothing
particular to interest me on shore, I
thought I would sail about a little
and see the watery part of the world.
\end{verbatim}

More precisely, you are given a sequence of words $S=(w_1, \dots, w_n)$ where $w_i$ consists of $c_i$ characters where each character is of the same width (a so-called ``fixed-width'' font such as Courier).  (Punctuation symbols are considered to be regular characters.)

The words must be placed in the order in which they appear in the sequence, $S$.
The maximum length of a line is $L$.  That is, a line can accommodate up to $L$ symbols, including the space that goes after each word on a line.  (Of course, there does not necessarily need to be a space added after the last word placed on a line.)

If words $w_i, \dots, w_j$ are placed on a line then the total length placed on that line, $\mbox{length}(i, j)$, is:
\[
 \mbox{length}(i, j) = \left( \sum_{k = i}^{j-1} (c_k + 1) \right) + c_j
\]
Notice that this accounts for the length of each of the words and the space immediately after the word.  The last word is treated separately since it does not need a space afterwards.

Recall that $\mbox{length}(i, j)$ must be at most $L$.  The \emph{slack} of that line is defined as 
$(L - \mbox{length}(i, j))$. 

Our objective is to pack the words in $S$ sequentially on lines so as to \emph{minimize the sum of the cubes of the slacks}, known as the \emph{penalty} of the packing.
We'll solve this with recursion first and then turn the recursive solution 
into a dynamic programming one.

Specifically, you should design a function
\texttt{pack}$(k)$ that returns the minimum penalty for packing words $1, \dots, k$.  Therefore, \texttt{pack}$(n)$ is the value of an optimal solution to our problem.

You should assume that the permitted width of the page, $L$, is a global variable that your recursive algorithm can consult.  There is really no need to keep passing that input into \texttt{pack}!

\begin{enumerate}
\item (2 points) First consider a simple greedy ``algorithm'':
  Pack as many words as will fit on the first line.  Then, move on to the next line and continue packing words, etc.  Give a small counterexample that shows that this approach gives solutions that are worse than optimal.  You'll need a small example, show the greedy solution on this example and the cost, and then show a better solution.
\item (10 points) Carefully and clearly describe a recursive algorithm  in English or pseudo-code for computing \texttt{pack}$(k)$.  For now, this algorithm should just return a number:  The optimal penalty.  The correctness of the algorithm should be quite  evident from the recursive structure of your algorithm, but you need not prove correctness here.
\item (10 points) Describe how a DP version of your algorithm would work.  Specifically, what is the shape of the table like, which elements can you fill in first, and how do you fill in the remainder of the table?
\item (6 points) Carefully derive the running time and space of your algorithm.  They may be polynomial in both $n$ and $L$. 
\item (2 points) Finally, explain how your algorithm can now be modified to produce the \emph{actual optimal packing} of words - that is a list of words to place on each line.   What is the total running time now?
\end{enumerate}

\item {\bf [40 Points] Block stacking}

For this part you should work in groups of 1 to 3 people.  Each group 
should submit
a single solution with the names of all members on it.  Groups should
not discuss their algorithms with one another, and it goes without
saying that groups should not share code.  However, if you would like
to share test files, that's ok (i.e., ``on this input our code
returned this output, what does your code do?'').  This kind of
collaboration should be documented in your submission.

The preferred languages are Python, Java, C, or C++.  Other high-level 
languages may also be fine, but you should check with me first.  In 
particular, it must be very easy for me to verify that your code 
implements the algorithm you describe.

%%%%%%%%%%%%%%%%%%%%
\subsubsection*{Problem Description}
The goal of this program is to build the tallest tower possible out of
a collection of rectangular blocks.  You are given some number of
types of blocks, each with its height, width, and length (all
positive integers)
specified.
You can use as many of each type of block as you would like, and a
block can be placed in any of the three stable orientations.  A block
can be stacked on top of another block if and only if the two
dimensions on the base of the top block are smaller than the two
dimensions on the base of the lower block.

We should be able to run your program from the command line with something
like (if you're using C or C++):
\begin{verbatim}
      % ./blocks <infile.txt> <outfile.txt>
\end{verbatim}
where {\tt $<$infile.txt$>$} is the name of an input file whose
contents are in the following form: 
\begin{verbatim}
       3
       2 6 8
       4 4 4
       1 10 4
\end{verbatim}
The first line specifies the number of block types.  Every other line 
specifies the dimension of a single block.  And {\tt
  $<$outfile.txt$>$} should be created by your code and
will contain the output of your program.

In more detail: your program should print to the screen the height of
the tallest tower it can build and the number of blocks it uses.  To
the output file your program should print out a list of blocks in the
same format as the input.  The first line specifies the number of
blocks in the tallest tower.  The rest of the lines give the blocks in
the tower, from the base (largest) to the tip (smallest), with the
height dimension last and the first two dimensions in the same order
for all the blocks (more specifically, the smaller of the two numbers
first).  With the above input the program should print:
\begin{verbatim}
       The tallest tower has 3 blocks and a height of 20
\end{verbatim}
and the output file should contain:
\begin{verbatim}
       3
       6 8 2
       2 6 8
       1 4 10
\end{verbatim}
Notice that the input file specifies block {\em types}, not individual
blocks, which explains why the $2 \times 6 \times 8$ block can appear
twice in the solution to the sample problem.

%%%%%%%%%%%%%%%%%%%%
\subsubsection*{Grading}
Your submission will be graded on three main criteria:
\begin{itemize}
\item correctness
   \begin{itemize}
   \item your algorithm solves the problem
   \item your code returns the correct solution to our test cases
     \begin{itemize}
     \item as before, we will be testing your code on the Pomona
       system.
     \item as before, we will be using the {\tt diff} command to
       verify correctness
     \item keep in mind that the number of blocks could be larger than 10, or
       100, or 1000, or \ldots.
     \end{itemize}
   \end{itemize}
\item clarity
   \begin{itemize}
   \item we understand why your algorithm solves the problem
   \item we understand how your code implements your algorithm
   \item we understand why the asymptotic running time is what
     you claim.
   \end{itemize}
\item running time
   \begin{itemize}
   \item the asymptotic bounds on your algorithm are not ridiculously
     inefficient (ie, if there's an $O(n^2)$ algorithm that isn't too
     difficult to think of, we don't want to see an $O(n^4)$ algorithm
     (or, even worse, an $O(2^n)$ algorithm))
   \item specific cases: your constants are not too ridiculous (we
     will consider actual running time for the test cases that we use)
   \end{itemize}
\end{itemize}

In addition we will be looking for evidence of sound problem solving,
as shown through your documentation.

%%%%%%%%%%%%%%%%%%%%
\subsubsection*{To Turn In}
You should submit the following either by putting everything in a
directory that we can access and mailing us a pointer to it, or by 
creating a web page with links to everything and emailing us a pointer 
to the web page.  It is your responsibility to make sure that your
links work and that your permissions are set correctly.
\begin{enumerate}
\item a README
  \begin{itemize}
  \item explain what each file you submitted is for
  \item explain how to compile and run your code
  \end{itemize}
\item Well-written documentation
  \begin{itemize}
  \item describe the algorithm that you implemented, argue that it is
    correct, and argue its expected running time.  
  \item describe an interesting design decision that you made
    (i.e. an alternative that you considered for your algorithm
    and why you decided against it).
  \item an overview of how the code you submit implements the
    algorithm you describe (e.g. highlights of central
    data structures/classes/methods/functions/procedures/etc . . .) 
  \item how you tested your code and the results of sample tests
  \item acknowledgements (if appropriate)
  \end{itemize}
\item Working code
  \begin{itemize}
  \item well-commented code implementing the algorithm you described
    in the documentation
  \end{itemize}
\end{enumerate}
\end{enumerate}

\end{document}
