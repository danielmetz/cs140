\documentclass[10pt]{article}

% page setup and prettification
\usepackage[hmargin=1.25in, vmargin=.7in]{geometry}
\usepackage{fancyhdr}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{lmodern,microtype,siunitx,parskip,xspace}

% important packages
\usepackage[shortlabels]{enumitem}
\usepackage{amsmath,amsthm,amssymb,mathtools,booktabs,commath,dsfont,%
  todonotes,xfrac,hyperref,mdframed,cleveref,subcaption,float}
\usepackage{titlesec}
\usepackage{minted}

% commands
\newcommand\todoin[2][]{\todo[inline, caption={2do}, #1]{ %inline todo box
  \begin{minipage}{\textwidth-4pt}#2\end{minipage}}}
\newcommand{\tinytodo}[2][size=\footnotesize]{ %smaller todo
  \todo[caption={#2}, #1]{\begin{spacing}{0.5}#2\end{spacing}}}
\newcommand\invisiblesection[1]{%
  \refstepcounter{section}%
  \addcontentsline{toc}{section}{\protect\numberline{\thesection}#1}%
  \sectionmark{#1}}
\newcommand{\problem}[3]{
  \invisiblesection{#1} \begin{mdframed}[] [#2 Points] #3 \end{mdframed}
  }

% definitions
\newcommand{\bigtheta}[1]{\ensuremath{\Theta(#1)}}
\newcommand{\bigo}[1]{\ensuremath{\mathcal{O}(#1)}}
\newcommand{\bigomega}[1]{\ensuremath{\Omega(#1)}}

% header
\pagestyle{fancy}
% \setlength{\headheight}{15pt} % to accomodate 12pt font
\lhead{CS 140 HW 10}
\chead{\leftmark}
\rhead{Daniel Metz}
\newcommand{\sectionbreak}{\clearpage} % force problems onto own pages


\begin{document}
\problem{Induction on Trees}{10}{
  Use induction to show that the number of degree-2 nodes in a non-empty binary
  tree is 1 less than the number of leaves. (Note that the book defines the
  degree of a vertex in a rooted tree as the number of children that it has.)
}

\input{cs140hw10p01.tex}

\sectionbreak
\problem{The Party Problem}{20}{
  You've been asked to design an algorithm for deciding who to invite to a
  company party. The structure of the company can be described by a tree as
  follows: the CEO is at the root, below the root are supervisors, below them
  are managers, below them are team leaders, etc., etc., until you get down to
  the leaves (summer interns). The tree is not necessarily binary; some non-leaf
  nodes may have one ``child,'' others two, and others even more.

  To make the party fun, we won't invite an employee along with their immediate
  boss (their parent in the tree). In addition, each person has been assigned a
  positive real number called their \emph{coefficient of fun}. The goal is to
  invite employees so as to maximize the total sum of the coefficients of fun of
  all invited guests, while not inviting an employee with his or her immediate
  boss.

  \begin{enumerate}[(a)]
  \item One possibility is to simply enumerate all possible subsets of her
    or his employees, throw out those subsets that include an employee and his
    or her boss, find the score for each remaining subset, and finally choose
    the best one. Assume there are 1000 employees and that you have a machine
    which can process one trillion (\(10^{12}\)) subsets per second. How long
    will it take to find the optimal solution using this brute force approach?
  \item Describe a recursive algorithm for this problem in clear English or
    clear pseudocode (with comments!). Assume that the tree is represented as a
    collection of nodes with links from parents to children and also from
    children to parents. The tree is passed to you by giving you a pointer to
    the root.
  \item Describe a DP algorithm for this problem. You may assume that each of
    the \(n\) nodes in the tree has a unique number between 1 and \(n\)
    associated with it. You may also assume that you have a function that will
    give you a linked list (or an array - whichever you wish) of all of the
    leaves in the tree. (This function simply uses a depth-first search to
    locate all of the leaves. In case you've forgotten, the running time of
    depth-first search on a tree is \bigo{n}, so this function is fast!)
  \item What is the asymptotic running time of your DP algorithm?  Explain.
  \item How can you modify your algorithm to find the optimal solution in which
    the CEO is invited to her own party?
  \end{enumerate}
}

\input{cs140hw10p02.tex}

\sectionbreak
\invisiblesection{Another MST Algorithm}
\begin{mdframed}[] [45 Points]
  The first algorithm for computing minimum spanning trees was published by the
  Czech mathematician Otakar Bar\.{u}vka in 1926 and was used for laying out
  electrical networks. It goes as follows:

\begin{verbatim}
  A = {};  \\ Comment:  A is a subset of a minimum spanning tree
  Consider the n  vertices in the graph as n connected components;
  while A contains fewer than n-1 edges
  {
     for each connected component C
     {
        Find the least weight edge (u,v) with one vertex in C and
          one vertex not in C;
          Indicate that edge (u,v) is "chosen" but do not add it yet to A;
     }
     Add all "chosen" edges to A;
     Compute the new connected components;
  }
  return A \\ Comment:  This is intended to be a MST!
\end{verbatim}

  Notice that if \(C_1\) and \(C_2\) are two different connected components
  before we begin the for loop, then inside the for loop the algorithm will
  choose the least weight edge coming out of component \(C_1\) and also the
  least weight edge coming out of \(C_2\). The edge chosen by \(C_1\) might
  join \(C_1\) and \(C_2\) into a new connected component, but this new
  connected component will not be discovered until the for loop has ended!  In
  other words, both \(C_1\) and \(C_2\) will each get an opportunity to choose
  the least weight edges coming out of their components.

  \begin{enumerate}[(a)]
  \item Give a counterexample that shows that Bor\.{u}vka's Algorithm doesn't
    work!! (You might find it useful to use the fact that some edges in the
    graph may have the same weight.) Show your counterexample graph and explain
    carefully why Bor\.{u}vka's Algorithm would not compute a minimum spanning
    tree in this case.
 \item Now assume that no two edges in the graph have the same weight. Such a
    graph has exactly one minimum spanning tree (you may just use this fact here,
    although you can think about how to prove it!) Under this assumption, prove
    that Bor\.{u}vka's Algorithm is correct.
  \item Why doesn't your proof from part (b) work if some edges in the graph
    have the same weights?
  \item How could Bor\.{u}vka's Algorithm be modified slightly to work in the
    most general case that edge weights are not necessarily distinct?  Explain
    briefly why this modification preserves the correctness of the algorithm.
  \item Describe an implementation of Bor\.{u}vka's Algorithm. The description
    should be in clear English,  indicate the data structures that would be used
    to support the algorithm and how they would be used, and give a careful
    derivation of the asymptotic worst-case running time using these data
    structures.
  \end{enumerate}
\end{mdframed}

\input{cs140hw10p03.tex}

\end{document}
