\documentclass[10pt]{article}

% page setup and prettification
\usepackage[hmargin=1in, vmargin=.7in]{geometry}
\usepackage{fancyhdr}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{lmodern,microtype,siunitx,parskip,xspace}

% important packages
\usepackage[shortlabels]{enumitem}
\usepackage{amsmath,amsthm,amssymb,mathtools,booktabs,commath,dsfont,%
  todonotes,xfrac,hyperref,mdframed,cleveref,subcaption,float}
\usepackage{titlesec}

% commands
\newcommand\todoin[2][]{\todo[inline, caption={2do}, #1]{ %inline todo box
  \begin{minipage}{\textwidth-4pt}#2\end{minipage}}}
\newcommand{\tinytodo}[2][size=\footnotesize]{ %smaller todo
  \todo[caption={#2}, #1]{\begin{spacing}{0.5}#2\end{spacing}}}
\newcommand\invisiblesection[1]{%
  \refstepcounter{section}%
  \addcontentsline{toc}{section}{\protect\numberline{\thesection}#1}%
  \sectionmark{#1}}
\newcommand{\problem}[3]{
  \invisiblesection{#1} \begin{mdframed}[] [#2 Points] #3 \end{mdframed}
  }

% definitions
\newcommand{\usd}[1]{\$#1}
\newcommand{\bigtheta}[1]{\ensuremath{\Theta(#1)}}
\newcommand{\bigo}[1]{\ensuremath{\mathcal{O}(#1)}}
\newcommand{\bigomega}[1]{\ensuremath{\Omega(#1)}}
\newcommand{\inner}[1]{\ensuremath{\left \langle #1 \right \rangle}}

% header
\pagestyle{fancy}
% \setlength{\headheight}{15pt} % to accomodate 12pt font
% \lhead{CS 140 HW 11}
\lhead{CS 140 HW 14 \quad \leftmark}
\rhead{Daniel Metz \& Charlie Watson}
\newcommand{\sectionbreak}{\clearpage} % force problems onto own pages


\begin{document}
\newcommand{\npcwarning}{
This assignment asks you to do several NP-completeness reductions. For each
reduction you need to:

  \begin{itemize}
  \item Clearly state what the decision problem is
  \item Show that the decision problem is in NP
  \item Describe a reduction, making sure to
    \begin{itemize}
    \item Argue that the reduction can be done in polynomial time.
    \item Explain why the reduction ``works'' (ie, why a ``yes'' maps to ``yes''
      and a ``no'' maps to ``no'')
    \end{itemize}
  \end{itemize}

Even if the explanation for one of these parts is a single sentence, you still
need to give it.}

\problem{Getting along}{20}{ In this problem you are given a set \(E = \cbr{e_1,
  \ldots, e_n}\) of \(n\) employees and a set \(H\) which consists of pairs of
  employees \del{e_i, e_j} such that employees \(e_i\) and \(e_j\) do not get
  along. In the optimization problem, we want to find the largest subset \(S
  \subseteq E\) such that for every pair of employees \(e_k, e_\ell \in S\),
  \(\del{e_k, e_\ell} \not \in H\).

  \begin{enumerate}[(a)]
  \item Formulate this as a decision problem.
  \item Show that the decision problem is in NP.
  \item Use \textsc{Clique} to prove that the decision problem is NP-complete.
  \end{enumerate} }

\input{cs140hw14p01.tex}

\newpage
\problem{\textsc{Hitting-Set}}{20}{ Assume you are given a set \(S\), and a
  collection \(C\) of subsets of \(S\). A hitting set for \(C\) is a subset \(S'
  \subseteq S\) such that \(S'\) contains at least one element from each subset
  in \(C\). The optimization problem asks for the size of the smallest hitting
  set.

  You can think of \(S\) as being a set of students, and of the \(C\) as a
  collection of subsets of \(S\) where all the students in a subset share some
  skill (eg ``good writer'', ``entertaining'', ``can code for 3 days straight'',
  ``has access to free pizza'', and so on). The hitting set, then, would be a
  study group whose members collectively possess every skill. The optimization
  problem asks how to minimize the size of the group.

  \begin{enumerate}[(a)]
  \item Formulate the \textsc{Hitting-Set} decision problem corresponding to the
    optimization problem described above.
  \item Show that \textsc{Hitting-Set} is in NP.
  \item Prove that \textsc{Hitting-Set} is NP-complete.
  \end{enumerate} }

\input{cs140hw14p02.tex}

\newpage
\problem{Dominating Set}{20}{ The \textsc{DominatingSet} problem is as follows:
  Given a graph \(G=(V,E)\) and a positive integer \(k\), does there exist a
  subset \(S \subseteq V\) where \(\abs{S} = k\) such that every vertex in \(V\)
  is either in \(S\) or is connected by an edge to some vertex in \(S\)?

  Take a close look at this problem to make sure that you understand how this
  problem differs from the seemingly similar \textsc{VertexCover} problem.

  Prove that \textsc{Dominating Set} is NP-complete. }

\input{cs140hw14p03.tex}

\newpage
\problem{The Partition Problem}{30}{
  \begin{enumerate}[(a)]
  \item Let \(S = \cbr{a_1, \ldots, a_n}\) and let \(f: S \to \mathbb{Z}^+\)
    (that is, \(f\) maps elements of \(S\) to the positive integers). A
    \emph{partition} of \(S\) is a pair of sets \(A\), \(B\) such that \(S = A
    \cup B\) and \(A \cap B = \emptyset\). The Partition Problem asks if there
    exists a partition of \(S\) into sets \(A\), \(B\) such that

      \[ \sum_{x \in A} f(x) = \sum_{x \in B} f(x) \]

    Prove that the Partition Problem is NP-complete using a reduction from a
    problem that we have seen in class or on a previous homework assignment.
    (Notice that while this definition of the problem is seemingly awkward, it
    permits us to have multiple items with the same value, that is  a
    ``multiset'' of numbers.)
  \item In fact, the Partition Problem remains NP-complete even if you are
    willing to tolerate a not-quite-exact split. The Partition100 Problem asks
    if there exists a partition of \(S\) into sets \(A\), \(B\) such that

    \[  \abs{\sum_{x \in A} f(x) - \sum_{x \in B} f(x)} \leq 100 \]

    Prove that the Partition100 problem is also NP-complete.
  \end{enumerate} }

\input{cs140hw14p04.tex}

\end{document}
