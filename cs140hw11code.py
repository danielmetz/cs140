import numpy as np

def FW(W):
    n = W.shape[0]
    D = [np.copy(W) for _ in xrange(n+1)]
    for k in xrange(n):
        D[k] = np.copy(D[k-1])
        for i in xrange(n):
            for j in xrange(n):
                D[k][i,j] = max(D[k-1][i,j], D[k-1][i,k] * W[k,j])
    return D[n-1]


    # M = np.copy(D)
    # n = M.shape[0]
    # for k in xrange(n):
    #     for i in xrange(n):
    #         for j in xrange(n):
    #             M[i,j] = max(M[i,j], M[i, * k]D[k, j])
    # return M




if __name__ == '__main__':
    M = np.matrix('1 .8 6.666666667; 1.25 1 10; .15 .1 1')
    # M = np.empty([5,5])
    # M[0,:] = [0, 3, 8, np.inf, -4]
    # M[1,:] = [np.inf, 0, np.inf, 1, 7]
    # M[2,:] = [np.inf, 4, 0, np.inf, np.inf]
    # M[3,:] = [2, np.inf, -5, 0, np.inf]
    # M[4,:] = [np.inf, np.inf, np.inf, 6, 0]
    # end = np.empty([3,3])
    # end[0,:] = []
    print 'input: '
    print M

    print '\n\nsuggested result'
    print FW(M)

    print '\n\nanswer below'
    ANS = np.matrix('1.2 .8 8; 1.5 1.2 10; .15 .12 1.2')
    print ANS

    print '\n\nbool matrix'
    print FW(M) == ANS