all: hw10

hw10: cs140hw10.tex cs140hw10p01.tex cs140hw10p02.tex cs140hw10p03.tex
	latexmk -pdf -shell-escape cs140hw10.tex                                                                        cs140/git/master !
	open cs140hw10.pdf

clean:
	latexmk -c cs140hw10.tex