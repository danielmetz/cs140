\documentclass[10pt]{article}

% page setup and prettification
\usepackage[hmargin=1.75in, vmargin=.65in]{geometry}
\usepackage{fancyhdr, parskip}
\pagestyle{fancy}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{lmodern, microtype, siunitx}

% important packages
\usepackage[svgnames,x11names]{xcolor}
\usepackage{amsmath, amsthm, amssymb, mathtools, booktabs, commath, dsfont,
setspace, todonotes, xfrac, hyperref, framed, siunitx, listings, tikz,
tikz-qtree, cleveref, outlines}
\usetikzlibrary{arrows}
\usepackage[shortlabels]{enumitem}

\lstset{showspaces=false, showstringspaces=false, basicstyle=\ttfamily\footnotesize}


% commands
\newcommand\todoin[2][]{\todo[inline, caption={2do}, #1]{ %inline todo box
  \begin{minipage}{\textwidth-4pt}#2\end{minipage}}}
\newcommand{\tinytodo}[2][size=\footnotesize]{ %smaller todo
  \todo[caption={#2}, #1]{\begin{spacing}{0.5}#2\end{spacing}}}

% definitions
\newcommand{\problem}[2][]{\begin{framed} \textbf{#1} #2 \end{framed}}
\newcommand{\indc}[2][-1]{\ensuremath{\mathds{1}\del[#1]{#2}}}
\renewcommand{\exp}{\operatorname{exp}\del}
\renewcommand{\Pr}{\operatorname{Pr}\del}
\newcommand{\E}{\operatorname{E}\del}
\newcommand{\given}[1][]{\:#1\vert\:}
\DeclarePairedDelimiter\ceil{\lceil}{\rceil}
\DeclarePairedDelimiter\floor{\lfloor}{\rfloor}

% header
\lhead{CS 140}
\chead{Assignment 3}
\rhead{Daniel Metz \& Natalie Casey}

\begin{document}

\problem[1. Solving Recurrences]{
  Give asymptotic upper and lower bounds for $T(n)$ in each of the following
  recurrences.Assume that $T(n)$ is constant for $n \leq 2$. Make your bounds as
  tight as possible (in other words, give $\Theta$ bounds where possible) and
  make sure to justify your answers. (Note: you may want to read appendix A.1
  in the textbook to remind yourself of basic summation properties.)

  \begin{enumerate}[(a)]
  \item $T(n) = 4T(n/2) + cn$
  \item $T(n) = T(n-1) + n$
  \item $T(n) = T(n-1) + 1/n$
  \item $T(n) = T(9n/10) + n$
  \item $T(n) = 2T(n/4) + \sqrt{n}$
  \end{enumerate}
}

\begin{enumerate}[(a)]
\item This question may be solved by use of the master theorem in its first
  case. Here we have $a = 4, b = 2$, and $f(n) \in \mathcal{O}(n^1)$. Thus
  solve for $\varepsilon$ in $\log_b a - \varepsilon = 1$ which gives
  $\varepsilon = 1$. This satisfies the master theorem requirement that
  $\varepsilon > 0$, and thus it follows that $T(n) \in \Theta(n^2)$.
\item This recurrence relation can be calculated quite easily as
  $\sum_{k=0}^{n-1} (n - k) = \sfrac{(n^2 + n)}{2}$, and thus is in
  $\Theta(n^2)$.
\item This recurrence relation is captured by $\sum_{k=0}^{n-1} \frac{1}{n-k} =
  \sum_{k = 1}^{n} \frac{1}{k}$. The textbook notes that this is equal to
  $\log n + \mathcal{O}(1)$, thus this recurrence is $\Theta(\log n)$.
\item Following the tree idea, we'll have a depth of $\log_{10/9} n$ and a width
  of 1 at each level. At the bottom, we'll have one leaf doing $d$ work. At each
  non-leaf level of the tree, the work done at level $k$ is $(\sfrac{9}{10})^k n$.
  Thus our total work is modeled by
  $d + n \sum_{k=0}^{\log_{10/9} n} (\sfrac{9}{10})^k$, which is upper-bounded
  by $d + n \sum_{k=0}^{\infty} (\sfrac{9}{10})^k = d + 10 n$ (geometric series).
  So far then we have that $T(n)$ is $\mathcal{O}(n)$. However, note that even
  in the first call we require $n$ work. Thus our bound is actually a tight lower-
  and upper-bound, and so $T(n)$ is $\Theta(n)$.
\item Following the tree idea again, we have a depth of $\log_4 n$ and a width
  at level $k$ of $2^k$ (where the root is level 0). At level $k$, the work done
  is $2^k \sqrt{\sfrac{n}{4^{k}}} = \sqrt{n}$. We can count the number of leaves
  to be $2^{\log_4 n}$. Thus our total work is $\sum_{k=0}^{\log_4 n} \sqrt{n} +
  2^{\log_4 n} d$. This simplifies to $\log_4 n \sqrt{n} + d \sqrt{n}$. In that
  expression, the first term dominates, thus $T(n)$ is $\Theta(\sqrt{n} \log n)$.
\end{enumerate}


\newpage
\problem[2. 3 Part Sort]{
  Consider the following sorting algorithm: First sort the first two-thirds of
  the elements in the array. Next sort the last two thirds of the array.Finally,
  sort the first two thirds again.Notice that this algorithm does not allocate
  any extra memory; all the sorting is done inside array $A$.Here's the code:

  \begin{tabbing}
  \hspace*{1cm} \= ThreeSort ($A$,$i$,$j$) \\
  \> \hspace*{.5cm} \= {\bf if} \= $A[i] > A[j]$ \\
  \>\>\> {\bf swap} $A[i]$ and $A[j]$ \\
  \>\> {\bf if} $i+1 \geq j$ \\
  \>\>\> {\bf return} \\
  \>\> $k = \lfloor (j-i+1)/3 \rfloor$. \\
  \>\> ThreeSort$(A,i,j-k)$ \ \ \ \ \ \= {\bf Comment:} Sort first two-thirds. \\
  \>\> ThreeSort$(A,i+k,j)$ \> {\bf Comment:} Sort last two thirds.\\
  \>\> ThreeSort$(A,i,j-k)$ \> {\bf Comment:} Sort first two-thirds again! \\
  \end{tabbing}

  \begin{enumerate}[(a)]
  \item Give an informal but convincing explanation (not a rigorous proof  by
    induction) of why the  approach of sorting the first two-thirds of the
    array, then sorting the last two-thirds of the array, and then sorting again
    the first two-thirds of the  array yields a sorted array.A few well-chosen
    sentences should suffice here.
  \item Find a recurrence relation for the worst-case running time of ThreeSort.
  \item Next, solve the recurrence relation using a recursion tree.
  \item Double check that you got the right answer in the previous part
    by solving the recurrence using the master theorem.
  \item How does the worst-case running time of ThreeSort compare with
    the worst-case running times of Insertion Sort, Selection Sort, and
    Mergesort?
  \end{enumerate}
}

\begin{enumerate}[(a)]
\item By the end of the first recursive call, we guarantee that any element
  wrongly initialized in the first third will be pushed into the second third
  for possible re-positioning in the next call. By the end of the second
  recursive call, we guarantee that any element wrongly initialized in the
  final third will be pushed into the second third for possible re-positioning.
  By now then, we know that any element that belongs in the final third is in
  its proper and final resting place. With the final recursive call, we complete
  the sorting of the first two-thirds of the array, which by merit of the
  previous sentence, we know to contain exactly those elements which it should.

\item In the worst-case (although really every case for ThreeSort), we perform
  constant work (checking if an element swap is needed, and doing so if
  appropriate) at each level of recursion, and we create three recursive calls,
  on \sfrac{2}{3}s of the elements in the current recursive level. This lends
  itself to a recurrence relation of
      \[ T(n) = 3 T \del{\frac{2}{3}} + c \]
\item
  \begin{figure} \centering
  \Tree [.$n$ [.{$\del{\frac{2}{3}} n$} [.{$\del{\frac{2}{3}}^2 n$} [.$\vdots$ ] [.$\vdots$ ] [.$\vdots$ ] ] [.{$\del{\frac{2}{3}}^2 n$} [.$\vdots$ ] [.$\vdots$ ] [.$\vdots$ ] ] [.{$\del{\frac{2}{3}}^2 n$} [.$\vdots$ ] [.$\vdots$ ] [.$\vdots$ ] ]] [.{$\del{\frac{2}{3}} n$} [.{$\del{\frac{2}{3}}^2 n$} [.$\vdots$ ] [.$\vdots$ ] [.$\vdots$ ] ] [.{$\del{\frac{2}{3}}^2 n$} [.$\vdots$ ] [.$\vdots$ ] [.$\vdots$ ] ] [.{$\del{\frac{2}{3}}^2 n$} [.$\vdots$ ] [.$\vdots$ ] [.$\vdots$ ] ]]  [.{$\del{\frac{2}{3}} n$} [.{$\del{\frac{2}{3}}^2 n$} [.$\vdots$ ] [.$\vdots$ ] [.$\vdots$ ] ] [.{$\del{\frac{2}{3}}^2 n$} [.$\vdots$ ] [.$\vdots$ ] [.$\vdots$ ] ] [.{$\del{\frac{2}{3}}^2 n$} [.$\vdots$ ] [.$\vdots$ ] [.$\vdots$ ] ]] ]
  \caption{Tree Diagram}
  \label{fig:tree}
  \end{figure}

  Looking at \Cref{fig:tree} at level $k$, we see that the work at level $k$ is
  equal to $3^k \cdot \del{\frac{2}{3}}^k n = 2^k n$. Further, we know that
  there are $\log_{3/2} n$ levels of the tree. Combining this with a branching
  factor of 3, at the end we're left with $3^{\log_{3/2} n}$ leaves. We can find
  the total then as

  \begin{align}
    3^{\log_{3/2} n} d + c \sum_{k = 0}^{\log_{3/2} n} 2^{k}
    & = d n^{\log_{3/2} 3} + c_1 2^{\log_{3/2} n + 1} - 1 \\
    & \leq d n^{\log_{3/2} 3} + c_2 n^{\log_{3/2} 2} \quad \mbox{where $c_2 > 2 c_1$}
  \end{align}

  Note that the RHS of (2) is $\Theta (n^{\log_{3/2} 3})$, as the first term
  outweighs the rest. This first term corresponds with the first term of (1), so
  if we consider only the first term, we can find a lower bound of $\omega
  (n^{\log_{3/2} 3})$ for our recurrence relation. Since (1) is also bounded
  above by (2), we now equal lower and upper bounds, so our recurrence relation
  is $\Theta (n^{\log_{3/2} 3})$.

\item From the master theorem, our $a = 3$, our $b = \sfrac{3}{2}$, and
  $f(n) = c$. First we must solve for $\varepsilon$ in $\log_b a - \varepsilon = 0$
  since $f(n)$ is $\mathcal{O}(n^0)$. Solving in this case gives $\varepsilon =
  \log_{3/2} 3 > 0$, which satisfies the condition required for the master
  theorem. Thus we have that $T(n)$ is $\Theta(n^{\log_b a}) =
  \Theta(n^{\log_{3/2} 3})$

\item From class discussion, we know the worst-case running times of Insertion
  Sort, Selection Sort, and Mergesort to be respectively $\mathcal{O}(n^2),
  \mathcal{O}(n^2), \& \mathcal{O}(n \log n)$. For ThreeSort, as discussed in
  (b), our worst-case (every-case) run-time is $\mathcal{O}(n^{\log_{3/2} 3})$.
  Thus worst-case scenarios ThreeSort is better than Insertion Sort and
  Selection Sort, yet still not as good as Mergesort.
\end{enumerate}

\newpage
\problem[3. Stock Market Problem (with code!)]{
  \begin{quote}
    The difference between art and science is that science is what we
    understand well enough to explain to a computer. Art is everything
    else. -- Don Knuth
  \end{quote}

  Recall that you're working at a brokerage firm which periodically
  examines how a particular stock has done in the last $n$ days.  This
  time you need to compute the longest consecutive number of days in
  which the stock's value did not decrease.  For example, consider the
  stock values below:

  \begin{center}
    \begin{tabular}{lrrrrrrrr} \toprule
    Day & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 \\
    Value & 42 & 40 & 45 & 45 & 44 & 43 & 50 & 49 \\ \bottomrule
    \end{tabular}
  \end{center}

  In this example, the length of the longest consecutive non-decreasing
  run is 3.  This run goes from day 2 to day 4.

  Here are your tasks:
  \begin{enumerate}[(a)]
  \item Briefly describe  a very simple ``naive'' algorithm for this problem and
    explain why the worst-case running time is $\Theta(n^2)$.
  \item Describe a divide-and-conquer algorithm whose running time is
    asymptotically better than $\Theta(n^{2})$.  Provide pseudocode
    and/or a clear English description of your algorithm.  (Note that
    your algorithm must be a divide-and-conquer algorithm.)
  \item Analyze the running time of your algorithm.
  \item Implement your divide-and-conquer algorithm in one of the following
    languages: C, C++, Java, or Python (ask an instructor first if you want to
    use another language). Your code must allow the user to specify two
    filenames, say \texttt{infile.txt} and \texttt{outfile.txt} on the command
    line (we don't want to edit your code). For example, if using C, your code
    might be executed as follows:
  \end{enumerate}

    \begin{center}
        \texttt{\% ./stockmarket infile.txt outfile.txt}
    \end{center}
    The contents of the input file will be in the
    following form:

    \begin{center}
      \begin{tabular}{l}
        8 \\
        42 \\
        40 \\
        45 \\
        45 \\
        44 \\
        43 \\
        50 \\
        49
    \end{tabular}
  \end{center}

  The first line specifies the number of days (which may not be a power of 2!).
  The following lines give the value of the stock on each day.

  In this case the output file should contain the following:

  \begin{center}
    \begin{tabular}{l}
        3 \\
        2 \\
        40 \\
        45 \\
        45
    \end{tabular}
  \end{center}

  The first line gives the length of the longest non-decreasing subsequence,
  the second line gives the day on which the subsequence begins (note the
  1-based indexing!), and the rest of the lines give the prices of the
  stock on the days included in the subsequence.

  On the written (in \LaTeX) problem set that you turn in on Tuesday, the
  solution to this part of the problem should consist of the following
  information:
  \begin{itemize}
  \item where to find your well-commented code (ie, a directory that I can
    access, a web page, dropbox, etc).
  \item how to compile and run your code (be very, very precise and keep in
    mind that it will be tested on \texttt{project2.cs.pomona.edu})
  \end{itemize}

  Most of the grade will be based on correctly implementing the algorithm you
  described in part 2 (so please make it easy for us to tell what your code is
  doing) and on whether the code computes the right answer for our test files
  (as determined by \texttt{diff}, so make sure your output  is in the format
  described above).We may also consider code efficiency in an absolute (wall
  clock) sense.

  (Small) sample input and output files will be on the course webpage.  Note
  that if there are two subsequences of equal length, either is an acceptable
  answer.
}

\begin{enumerate}[(a)]
\item A ``naive'' algorithm would be one that considers each possible start
  point (index 0, index 1, \ldots), then extends the chain (starting at that
  start point) until the non-decreasing condition has been broken. In the
  worst-case (chain is an entirely non-decreasing sequence), we are constructing
  chains of length $n, n-1, n-2, \ldots 1$, each which takes work
  linearly-proportionate to the chain length. $\sum_{k=0}^{n-1} (n - k)$. This
  calculation of our work is exact and is therefore $\Theta(n^2)$.
\item
\begin{lstlisting}[language=Python]
def max_subarray(prices, low, high):
    """
    Finds the longest non-decreasing subarray between low and high.

    inputs:
        prices (numeric array-like): array to be searched
        low (int): lowest permitted index for search
        high (int): highest permitted index for search
    returns:
        (int) optimal starting index
        (int) optimal ending index
        (int) length of maximum non-decreasing subarray that crosses mid

    attributions: code heavily inspired by Find-Maximum-Subarray algorithm in
        Cormen textbook
    """
    # base case: no more than one element
    if high <= low:
        return low, high, max(high - low, 0)

    # calculate the mid-point
    mid = (low + high) / 2
    # recurse on left-subarray
    left_low, left_high, left_sum = max_subarray(prices, low, mid)
    # recurse on right-subarray
    right_low, right_high, right_sum = max_subarray(prices, mid + 1, high)
    # do a middle-out search for mid-crossing sequences
    cross_low, cross_high, cross_sum = max_crossing(prices, low, mid, high)

    # collect together results
    lows = left_low, right_low, cross_low
    highs = left_high, right_high, cross_high
    sums = left_sum, right_sum, cross_sum

    # find the best result
    choice = sums.index(max(sums))

    # return relevant details describing the best result
    return lows[choice], highs[choice], sums[choice]


def max_crossing(prices, low, mid, high):
    """
    Finds the longest non-decreasing subarray between low and high that must
    cross mid. Helper function to max_subarray

    inputs:
        prices (numeric array-like): array to be searched
        low (int): lowest permitted index for search
        mid (int): mid-point index that must be crossed
        high (int): highest permitted index for search
    returns:
        (int) optimal starting index
        (int) optimal ending index
        (int) length of maximum non-decreasing subarray that crosses mid

    attributions: code heavily inspired by Find-Max-Crossing-Subarray algorithm
        in Cormen textbook
    """

    # iterative expansion from midpoint leftward
    left_runner = mid
    while left_runner >= low and while the subsequence remains non-decreasing:
        # expand so long as sequence remains non-decreasing
        # check first across mid-bridge to ensure crossing is logical
        left_runner -= 1
    # increment it so that it goes back to the last value that kept the
    # subsequence non-decreasing
    left_runner += 1

    # iterative expansion from midpoint rightward
    right_runner = mid + 1
    while right_runner <= high and while the subsequence remains non-decreasing:
        # expand so long as sequence remains non-decreasing
        # check first across mid-bridge to ensure crossing is logical
        right_runner += 1
    # decrement it so that it goes back to the last value that kept the
    # subsequence non-decreasing
    right_runner -= 1

    return left_runner, right_runner, right_runner-left_runner+1
\end{lstlisting}

\item Looking at our pseudocode (or better, code if skipping to step (d) first),
  we see that we make two recursive calls to \texttt{max\_subarray}, each with
  an input of half the size of the current iteration's size. Furthermore, we
  make one call to \texttt{max\_crossing}. These three calls aside, all work
  done in \texttt{max\_subarray} is $\mathcal{O}(1)$.

  Looking at \texttt{max\_crossing}, we see that in the worst-case, the entire
  sub-sequence from low to high is non-decreasing, resulting in \sfrac{$n$}{2}
  checks as it expands leftward and \sfrac{$n$}{2} checks as it expands rightward.
  We conclude then that \texttt{max\_crossing} is $\Theta(n)$.

  This then gives us our recurrence relation:
    \[ \begin{aligned}
      T(n) & = 2 T \del{\frac{n}{2}} + c n  \\
      T(1) & = d
    \end{aligned} \]

  We envisioned a tree-diagram to represent our recurrence relation. This tree
  has a branching factor of 2 and at level $k$ (where the root is level 0)
  the input size is $\del{\sfrac{1}{2}}^k n$. This tells us that the work being
  done at level $k$ is equal to $\sum_{j = 1}^{2^k} \del{\del{\sfrac{1}{2}}^k
  c n} = c n$. Our tree depth is $\log_2 n$, thus our non-leaf work is equal to
  $\sum_{k = 0}^{\log_2 n} c n = c n \log n$. As for our leaf work, we calculate
  that there are $2^{\log_2 n} = n$ leaves, for which we know that each leaf
  does constant work. Thus our leaves contribute $d n$ work.

  Our total worst-case work then is $c n \log n + d n$. Since we have made no
  estimations here (beyond permitted ignoring of floors and ceilings), we
  conclude that our algorithm is $\Theta(n \log n)$.

\item File may be found at \url{https://goo.gl/kzL14n} (direct link to file
    uploaded to a bitbucket repo). Alternatively, in case the first link fails,
    \url{https://goo.gl/07LFaz} (link to fire shared via dropbox) should work.
    Futhermore, the file will also be uploaded to Sakai's dropbox.

    Usage instructions are included in the file itself, but also provided here.
    Either of the two methods should be adequate.

    First, aquire the file via one of the following options:
    \begin{outline}
    \1 manually retreive the file via one of the links above, making sure to
      save the file with extension \texttt{.py}
    \1 \texttt{> wget https://goo.gl/kzL14n}
    \1 \texttt{> curl https://goo.gl/kzL14n > cs140hw03p3.py}
    \end{outline}

    Next, run the program using either of the following methods:
    \begin{outline}
    \1 make executable method
        \2[1.)] \texttt{> chmod +x cs140hw03p3.py}
        \2[2.)] \texttt{> ./cs140hw03p3.py infile outfile}
    \1 run as python script
        \2[1.)] \texttt{> python cs140hw03p3.py infile outfile}
    \end{outline}
\end{enumerate}

\newpage
\problem[4. Chip Testing (Bonus)]{
  You have $n$ supposedly identical computer chips that in principle are capable
  of testing each other. Your test jig accommodates two chips at a time. When
  the jig is loaded, each chip tests the other and reports whether it is good or
  bad. A good chip always reports accurately whether the other chip is good or
  bad, but the answer of a bad chip cannot be trusted. Thus, the four possible
  outcomes of a test are as follows:

  \begin{center}
    \begin{tabular}{lll} \toprule
    Chip A says & Chip B says & Conclusion \\ \midrule
    B is good & A is good & Both good, or both bad \\
    B is good & A is bad & At least one is bad \\
    B is bad & A is good & At least one is bad \\
    B is bad & A is bad & At least one is bad \\ \bottomrule
    \end{tabular}
  \end{center}

  \begin{enumerate}[(a)]
    \item Show that if more than $\frac{n}{2}$ chips are bad, you cannot
      necessarily determine which chips are good using any strategy based on
      this kind of pairwise test. Assume that the bad chips can conspire to fool
      you.
    \item Consider the problem of finding a single good chip from among $n$
      chips, assuming that more than $\frac{n}{2}$ of the chips are good. Show
      that $\floor{\sfrac{n}{2}}$ pairwise tests are sufficient to reduce
      the problem to one of nearly half the size.
    \item Show that the good chips can be identified with $\Theta(n)$ pairwise
      tests, assuming that more than $\frac{n}{2}$ of the chips are good.
      Explain clearly why your algorithm identifies all of the good chips. Then
      give and solve the recurrence that describes the number of tests.
  \end{enumerate}
}


\end{document}
















