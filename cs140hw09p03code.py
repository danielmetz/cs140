MAX_LENGTH = 24

def opt_penalty(remainder, words_left):
	# base case: no words left
	if not words_left:
		return remainder ** 2

	# there are words left: let's grab it
	next_word = words_left[0]
	word_length = len(next_word)

	# case where we cannot fit the next word on this line
	if 1 + word_length > remainder:
		return remainder ** 2 + \
			opt_penalty(MAX_LENGTH - word_length, words_left[1:])

	# case where it does fit
	# two choices: either put next_word on current line or on a new line

	# put next_word on current line
	same_line = opt_penalty(remainder - 1 - word_length, words_left[1:])

	# put next_word on a new line
	new_line = remainder ** 2 + \
		opt_penalty(MAX_LENGTH - word_length, words_left[1:])

	return min(same_line, new_line)

if __name__ == '__main__':
	words = 'hello cruel pretty world hi'.split()
	print words
	print opt_penalty(0, words)