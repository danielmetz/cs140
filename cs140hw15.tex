\documentclass[10pt]{article}

% page setup and prettification
\usepackage[hmargin=1in, vmargin=.7in]{geometry}
\usepackage{fancyhdr}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{lmodern,microtype,siunitx,parskip,xspace}

% important packages
\usepackage[shortlabels]{enumitem}
\usepackage{amsmath,amsthm,amssymb,mathtools,booktabs,commath,dsfont,%
  todonotes,xfrac,hyperref,mdframed,cleveref,subcaption,float,tabu}
\usepackage{titlesec}

% commands
\newcommand\todoin[2][]{\todo[inline, caption={2do}, #1]{ %inline todo box
  \begin{minipage}{\textwidth-4pt}#2\end{minipage}}}
\newcommand{\tinytodo}[2][size=\footnotesize]{ %smaller todo
  \todo[caption={#2}, #1]{\begin{spacing}{0.5}#2\end{spacing}}}
\newcommand\invisiblesection[1]{%
  \refstepcounter{section}%
  \addcontentsline{toc}{section}{\protect\numberline{\thesection}#1}%
  \sectionmark{#1}}
\newcommand{\problem}[3]{
  \invisiblesection{#1} \begin{mdframed}[] [#2 Points] #3 \end{mdframed}
  }

% definitions
\newcommand{\usd}[1]{\$#1}
\newcommand{\bigtheta}[1]{\ensuremath{\Theta(#1)}}
\newcommand{\bigo}[1]{\ensuremath{\mathcal{O}(#1)}}
\newcommand{\bigomega}[1]{\ensuremath{\Omega(#1)}}
\newcommand{\inner}[1]{\ensuremath{\left \langle #1 \right \rangle}}
\DeclarePairedDelimiter\ceil{\lceil}{\rceil}

% header
\pagestyle{fancy}
% \setlength{\headheight}{15pt} % to accomodate 12pt font
% \lhead{CS 140 HW 11}
\lhead{CS 140 HW 15}
\rhead{Daniel Metz \& Charlie Watson}
\newcommand{\sectionbreak}{\clearpage} % force problems onto own pages


\begin{document}

\begin{mdframed}
  \textbf{1.} [25 Points] \textbf{Another approximation algorithm for Vertex
  Cover}

  Recall that in class we discussed a 2-approximation algorithm for the Vertex
  Cover problem - that is, an algorithm that finds solutions that are at most 2
  times as large as optimal. In this problem, we'll use a different (and
  extremely versatile) approach to finding a 2-approximation algorithm for the
  same problem.

  Here's the idea. We first reduce the Vertex Cover optimization problem into an
  Integer Linear Programming optimization problem. We do so by introducing a
  variable \(x_i\) for each vertex \(v_i\) in the graph. We constrain \(x_i\) to
  be 0 or 1 in the ILP to represent not including or including, respectively,
  the vertex \(v_i\) in the vertex cover. For every edge \(\{v_i, v_j\}\) in the
  graph, we introduce the constraint \(x_i + x_j \geq 1\) to indicate that at
  least one of the two corresponding vertices must be selected for the vertex
  cover. Finally, we wish to minimize the objective function \(x_1 + \cdots +
  x_n\).

  The problem, of course, is that we're now left with an ILP problem and ILP is
  itself NP-complete. So here's what we do: solve the problem using a
  polynomial-time \emph{Linear Programming} algorithm (e.g. an interior point
  method). Notice that the integrality constraint is now relaxed, which is why
  this general technique is called ``Linear Programming Relaxation''.
  Unfortunately, the solution found by the linear programming algorithm will
  permit each \(x_i\) to be some arbitrary rational number between 0 and 1. We
  now round each \(x_i\) in the linear programming solution to its nearest
  integer value:  If \(x_i < 0.5\) we round it down to 0 and if \(x_i \geq 0.5\)
  we round it up to 1. (Notice the slight asymmetry in the rounding algorithm---
  that's important!). The rounded values tell us which vertices should be in the
  vertex cover.

  \begin{enumerate}[(a)]
  \item Consider the graph in \Cref{fig:pr1}:
    \begin{enumerate}[i)]
    \item Give the ILP formulation (in standard form) for minimum vertex cover
    on this graph.
    \item How does the formulation change in the LP formulation?
    \item What will the solution to the ILP be?
    \item What's an example of a point that's feasible (though possibly not
    optimal) for the LP formulation that is not feasible for the ILP
    formulation?
    \end{enumerate}

  \item Having looked at a small example, now go back to the overall LP
    relaxation algorithm described above. Show that, for any graph, the
    algorithm always gives a solution that corresponds to a valid vertex cover.

  \item Now show that, for any graph, the vertex cover given by the LP
    relaxation algorithm is at most twice as large as the optimal vertex cover.
    (Hint: relate the quality of the LP solution to that of the optimal
    solution, then the quality of the rounded solution to that of the LP
    solution)
  \end{enumerate}
\end{mdframed}

\begin{figure}[!h] \centering
  \includegraphics[height=1.2in]{cs140ps15img_vc_approx.pdf}
  \caption{Figure for Problem 1}
  \label{fig:pr1}
\end{figure}

\begin{enumerate}[(a)]
\item \begin{enumerate}[i)]
  \item
    Let us define \(\mathbf{x} = (x_1, x_2, x_3, x_4) = (u, x, y, z)\). Then
    \begin{table}[H] \centering
    \begin{tabu} to 80mm {r X[1,r,p,$] X[1,r,p,$] X[1,r,p,$] X[1,r,p,$] X[2,l,p,$]}
    Maximize   & - x_1 & - x_2 & - x_3 & - x_4  & ~ \\
    Subject to & - x_1 & - x_2 & ~     &        & \leq -1 \\
    ~          & - x_1 & ~     & - x_3 &        & \leq -1 \\
    ~          & - x_1 & ~     & ~     & - x_4  & \leq -1 \\
    ~          & ~     & ~     & - x_3 & - x_4  & \leq -1 \\
    ~          &   x_1 & ~     & ~     & ~      & \leq 1 \\
    ~          & ~     &   x_2 & ~     & ~      & \leq 1 \\
    ~          & ~     & ~     &   x_3 & ~      & \leq 1 \\
    ~          & ~     & ~     & ~     &   x_4  & \leq 1 \\
    ~          &   x_1,&   x_2,&   x_3,&   x_4  & \geq 0 \\
    ~          &   x_1,&   x_2,&   x_3,&   x_4  & \in \mathbb{Z}
    \end{tabu}
    \end{table}
  \item
    For the LP, we simply relax the ILP, removing the integer condition.
  \item
    The solution to the ILP will be \(\mathbf{x} = (u, x, y, z) = (1, 0, 1, 0)\)
    with associated cost equal to 2.
  \item
    The solution \(\mathbf{x} = (u, x, y, z) = (.5, .5, .5, .5)\) is a solution
    feasible for the LP but not the ILP. Note that it satisfies all of our
    constraints of the LP (equivalently, it satisfies all constraints of the ILP
    except the integer constraint, obviously and so it does not satisfy the
    ILP). \footnote{Note if we then round this answer (rounding such that 0.5
    rounds to 1, as specified in the problem statement), we end up including all
    vertices to compose our cover, giving the trivial and very non-optimal
    solution.}
  \end{enumerate}
\item Any solution given as a solution to the LP relaxation must itself be a
  feasible solution. This means that the provided solution must satisfy all the
  constraints set-up in our LP formulation. For every two vertices in our graph
  such that there exists an edge between them, we have in our set of constraints
  a constraint that says the sum of the ``vertex weights'' must be at least 1.
  In the worst-case then, the worst-case being the case in which we're least
  likely  to have created a vertex cover, the two ``vertex weights'' will sum to
  exactly 1. Suppose then that the two weights respectively are \(p\) and \(1
  - p\) for \(0 \leq p \leq 1\). Then \(\text{round}(p) + \text{round}(1 - p)
  \geq 1\) as at least one of \(p\) and \(1 - p\) must be at least \sfrac{1}{2}.
  Since we have such a situation for \emph{every} edge and its incident
  vertices, we know that for every edge, we'll be including at least one of the
  two incident vertices in a feasible solution found by the LP relaxation. This,
  being consistent with the definition of a vertex cover, shows the LP
  relaxation solution to be a valid vertex cover.
\item Since the LP relaxation solves a problem with fewer constraints than the
  ILP, and the ILP is formulated so that the optimal solution to the ILP gives
  the smallest vertex cover, the solution generated by the LP relaxation
  (pre-rounding) can be no worse than the true optimal solution (after all,
  it's solving an easier problem). When we round a solution given by the relaxed
  LP, either we include one of the two incident vertices for each edge
  (necessary for a proper vertex cover as coded into our constraints) or we
  happen to choose both (if for instance the weights for the two vertices
  are both \sfrac{1}{2}). The worst-case then is if for every edge in the LP
  solution, rounding ends up including both incident vertices. This however is
  at worst doubling the number of vertices required to solve the LP. Recall that
  the LP is no worse than the optimal solution, so double this can be no worse
  than twice the optimal solution. Thus the solution generated by an LP
  relaxation algorithm as described is a 2-approximation algorithm.
\end{enumerate}


\end{document}
