\documentclass[10pt]{article}

% page setup and prettification
\usepackage[hmargin=1.5in, vmargin=.75in]{geometry}
\usepackage{fancyhdr, parskip}
\pagestyle{fancy}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{lmodern, microtype, siunitx}

% important packages
\usepackage[svgnames,x11names]{xcolor}
\usepackage{amsmath,amsthm,amssymb,mathtools,booktabs,commath,dsfont,
setspace,todonotes,xfrac,hyperref,framed,siunitx,listings,tikz,
tikz-qtree,cleveref,outlines,xspace}
\usetikzlibrary{arrows}
\usepackage[shortlabels]{enumitem}

\lstset{showspaces=false, showstringspaces=false, basicstyle=\ttfamily\footnotesize}


% commands
\newcommand\todoin[2][]{\todo[inline, caption={2do}, #1]{ %inline todo box
  \begin{minipage}{\textwidth-4pt}#2\end{minipage}}}
\newcommand{\tinytodo}[2][size=\footnotesize]{ %smaller todo
  \todo[caption={#2}, #1]{\begin{spacing}{0.5}#2\end{spacing}}}

% definitions
\newcommand{\problem}[2][]{\begin{framed} \textbf{#1} #2 \end{framed}}
\newcommand{\indc}[2][-1]{\ensuremath{\mathds{1}\del[#1]{#2}}}
\renewcommand{\exp}{\operatorname{exp}\del}
\renewcommand{\Pr}{\operatorname{Pr}\del}
\newcommand{\E}{\operatorname{E}\del}
\newcommand{\given}[1][]{\:#1\vert\:}
\DeclarePairedDelimiter\ceil{\lceil}{\rceil}
\DeclarePairedDelimiter\floor{\lfloor}{\rfloor}
\newcommand{\bigtheta}[1]{\ensuremath{\Theta(#1)}}
\newcommand{\bigo}[1]{\ensuremath{\mathcal{O}(#1)}}
\newcommand{\bigomega}[1]{\ensuremath{\Omega(#1)}}

\newcommand{\NULL}{\texttt{NULL}\xspace}
\newcommand{\push}{\texttt{push}\xspace}
\newcommand{\pop}{\texttt{pop}\xspace}
\newcommand{\enqueue}{\texttt{enqueue}\xspace}
\newcommand{\dequeue}{\texttt{dequeue}\xspace}
\newcommand{\findmin}{\texttt{find-min}\xspace}

% header
\setlength{\headheight}{15pt} % to accomodate 12pt font
\lhead{CS 140}
\chead{Assignment 6}
\rhead{Daniel Metz \& Huu Le}

\begin{document}

\problem[1. Stacks and Queues]{
  Assume you're given an implementation of a stack that supports \texttt{push}
  and \texttt{pop} in \bigo{1} time.  Now you'd like to implement a queue using
  these stacks.

  \begin{enumerate}[(a)]
  \item {[6 points]} Explain how you can efficiently implement a queue using two
    of these stacks.  (``Efficiently'' means in a way that allows you to do the
    next part of the problem.)
  \item {[24 points]} Prove that the amortized cost of each \texttt{enqueue} and
    \texttt{dequeue} operation is \bigo{1} for your stack-based queue.  So that
    you practice using each technique, you should give three separate proofs,
    each using a different one of the three amortized analysis techniques
    (aggregate, accounting, and potential).
  \end{enumerate}
}

\begin{enumerate}[(a)]
\item Suppose we have two stacks, stack A and stack B. Any \push will push the
    item onto stack A. Any \pop will check to see if stack B has non-zero size.
    If so, \pop will then proceed to pop-off the top element on the B stack.
    Otherwise, \pop will pop-off every element in stack A onto stack B. In
    effect, this process has reversed the order of the elements in stack A in
    moving them to stack B, allowing a FIFO type operation.
\item \begin{enumerate}[(i)]
    \item \textbf{Aggregate.} For each \enqueue, each element merely needs to be
    pushed onto stack A. This is a \bigo{1} operation, and thus \enqueue will be
    a \bigo{1} operation. For each \dequeue, each element that has already been
    enqueued will need to be popped off stack A and pushed onto stack B, then
    popped off stack B. Given each of these is a \bigo{1} operation, these three
    steps for dequeue will total three units of work, putting it in the class of
    a \bigo{1} operation.
    \item \textbf{Accounting.} For each \enqueue, we ``spend'' \$4, but we only
    ``use'' \$1 in the actual \push operation leaving \$3 stored on each
    element. For each \dequeue, when moving elements from stack A to stack B, we
    use \$2 (of the \$3 stored on each element), \$1 going towards the \pop off
    of stack A and \$1 going towards the \push onto stack B. Finally, a \pop off
    of stack B will use the final \$1 remaining on the element finishing its
    passage through the queue. At no point has any element gone into ``debt'' as
    it passes through the ``queue'' (stack pair). Because the amount ``spent''
    on each element in its insertion is a constant (independent of $n$), the
    total work divided up between \enqueue and \dequeue is a constant, and thus
    each process separately must also be \bigo{1}.
    \item \textbf{Potential.} Let
        \[ \Phi(D_i) = 3\ (\mbox{\# elements on stack A}) + \ (\mbox{\# elements
        on stack B}). \]

    Then for a sequence of $n$ \enqueue operations, each operation costs 1 unit
    of work each, and for the $i$th push, $\Phi(D_i) = 3i$, and $\Phi(D_{i - 1})
    = 3 (i - 1)$. For $i = 1, \ldots, n$ $\Phi(D_i)$ is always non-negative and
    $\Phi(D_0) = 0$. Furthermore, at every step, we have $c_i + \Phi(D_i) -
    \Phi(D_{i - 1}) = 1 + 3i - 3(i - 1) = 4$. This is \bigo{1} amortized work
    per operation.

    For a sequence of $n$ \dequeue operations, where all that's been done
    beforehand is $n$ \enqueue operations, we recognize that all $n$ elements
    exist on stack A. (Note, we justify this assumption by recognizing that if
    not all $n$ elements are on stack A, then there must have been a call to
    \pop which violates our premise that no \dequeue operations have yet been
    called.) The first call to \dequeue requires popping all elements off stack
    A, pushing all elements onto stack B, and popping off the item now at the
    top of stack B. All other calls to \dequeue require only constant work. For
    the former case, we have $c_1 = 2n + 1$, $\Phi(D_1) = n - 1$, and $\Phi(D_0)
    = 3n$ which when appropriately combined yield 0. For the latter case ($i =
    2, \ldots, n$), we have $c_i = 1$, $\Phi(D_i) = k - i$, $\Phi(D_{i - 1}) = k
    - i + 1$ which together combine to yield 0, again. Since we've determined
    the work for each step in $i = 1, \ldots, n$ is zero, the amortized work is
    \bigo{1} per operation.

\end{enumerate}
\end{enumerate}


\newpage
\problem[2. Minqueues]{
  A Minqueue is an abstract data type (ADT) that supports the following
  operations:

  \begin{description}
  \item{\textsc{enqueue}($x$):}  Inserts the number $x$ into the Minqueue.
  \item{\textsc{dequeue}():}  Removes the element that has been in the Minqueue
    for the longest time.
  \item{\textsc{find-min}():}   Returns the smallest value in the Minqueue but
    \emph{does NOT} remove it from the Minqueue.
  \end{description}

  \begin{enumerate}[(a)]
  \item  A Minqueue can be implemented using a simple doubly linked list data
    structure where \textsc{enqueue}ing is done in constant time by adding to
    the end of the queue, \textsc{dequeue}ing is done in constant time by
    removing the element at the front of the queue, and \textsc{find-min} is
    performed by searching through the linked list for the smallest element.
    Show that with this implementation of a Minqueue, there exists some sequence
    of $n$ operations (i.e., some sequence of \textsc{enqueue},
    \textsc{dequeue}, and \textsc{find-min}) that takes $\Omega(n^2)$ time.
  \item But a Minqueue can also be implemented using a clever data structure
    which we'll refer to as ``Real Queue and Helper Queue.''  Here's how it
    works: When an element is enqueued, it is placed into a regular queue
    (implemented as a doubly linked list).   However, it is \emph{also} placed
    at the tail of a special ``helper queue'' (also implemented as doubly linked
    list).  The helper queue will always contain a subset of those elements in
    the real queue in sorted order from small elements at the front to large
    elements in the back. In particular, when an element  $x$ is inserted at the
    tail end of the helper queue, it checks to see if it is smaller than the
    element right in front of it in the helper queue.  If so, it removes the
    element in front of it in the helper queue and again compares itself to its
    predecessor.  It repeatedly removes its predecessors until it gets to a
    point that its predecessor is smaller than it!  To dequeue, we simply remove
    that element from the head of the real queue.  We also check to see if it is
    at the head of the helper queue, in which case we remove it from that queue
    as well.  Finally, to determine which element is the minimum, we simply look
    at the front of the helper queue.

    Use the accounting method to prove that the total running time of \emph{any}
    sequence of $n$ operations in time \bigo{n} time (or, as we sometimes say,
    \bigo{1} amortized time per operation).
  \end{enumerate}
}

\begin{enumerate}[(a)]
\item (Assumption: Minqueue is empty at our start.) Consider the sequence of
    $n$ operations composed of \sfrac {n} {2} \enqueue operations followed by
    \sfrac{n}{2} \findmin operations. The total work can be calculated as
    follows:

    \[ \begin{aligned}
    & \sum_{i = 1}^{n/2} 1 + \sum_{i = i}^{n/2} \frac{n}{2} \\
    & = \frac{n}{2} + \frac{n}{2} \cdot \frac{n}{2} \\
    & = \frac{n}{2} + \frac{n^2}{4}
    \end{aligned} \]

    Given this work is of order $n^2$, we have shown that there exists a
    sequence of $n$ operations that takes order $n^2$ time, establishing a
    lower-bound for the possible worst-case complexity of $n$ operations. This
    is the definition of an $\Omega$-bound, so we conclude \bigomega{n^2}.
\item We begin by noting the costs we assign to each operation:
    \begin{itemize}
    \item \enqueue: \$5
    \item \dequeue: \$2
    \item \findmin: \$1
    \end{itemize}

    We now justify the costs.

    For \enqueue, we pay \$1 to insert the element into the regular queue. We
    also pay \$1 to insert the element into the helper queue. We pay \$1 to
    compare the element we just added in the helper queue to the
    preceding element in the helper queue (even if \NULL). This leaves \$2 in
    the hands of the element we just inserted. This is our invariant: that every
    element in the helper queue has \$2 in its hands.

    In the case where the comparison told us that the element just inserted is
    properly in its place, we end here. If however the comparison tells us that
    the element just inserted must move forward in the helper queue (case where
    inserted element is smaller than its preceding element), we use \$1 from
    the \$2 the preceding element has to remove this element from the helper
    queue, and we use its final remaining dollar to allow the element taking its
    place to compare itself to the new preceding element. At this point, this
    deletion and comparison process continues until a comparison asserts that
    the element inserted is now in its proper place.

    Note that at no point may an element in the helper queue have fewer than
    \$2, at the end of the \enqueue function call. As a result, at no point may
    any call to \enqueue result in going into ``debt.''

    For \dequeue, we pay \$2. \$1 gets spent to remove the first element in the
    regular queue. The other \$1 gets spent to check if the first element of the
    helper queue is the same as the element we just removed. If not, we end
    here. If so, we use the \$2 contained on this first element in the helper
    queue as follows: \$1 gets spent to remove this element from the helper
    queue, and the other \$1 gets spent to compare the new first element of the
    helper queue against \NULL. Again, at no point may any call to \dequeue
    result in going into debt.

    For \findmin, we pay \$1. This \$1 gets immediately spent just to check the
    first element of the helper queue and report its value. We assume that it
    costs no more to report that the helper queue is empty if this is the case.

    Because \enqueue, \dequeue, and \findmin were each designed with constant
    costs (free of $n$), each operation is an amortized \bigo{1} operation,
    allowing us to conclude that any sequence of $n$ operations may be completed
    in \bigo{n} time.
\end{enumerate}


\end{document}
















