# README

	authors: Daniel Metz and Charlie Watson

## Submitted Files

	* cs140hw09p04code.py: the CODE
	* README.txt: this file

## How To Run

	python cs140hw09p04code.py infile outfile

## Documentation

### Describe the Algorithm

	1. We begin by generating a block for every orientation of each block. Given
	each block has three dimensions, there are 3! = 6 possible orderings of
	these dimensions.

	2. We then proceed to sort these blocks based upon their dimensions. A block
	is considered to be in order (dim_one, dim_two, height), and we order
	(ascendingly) first by dim_one, then by dim_two, then by height.

	3. We begin by initializing a height and a parent array, each of width equal
	to the number of parents. They are respectively initialized to zero and
	None. These will in the end represent stacks with the block at this index
	being the base. The height obviously corresponds to the height of the stack
	and the parent corresponds to the optimal block to be placed atop the
	current block under consideration.

	4. To determine the value to be placed into the height and parent arrays for
	a particular block, we consider the blocks that proceed this one in our
	ordering. For each block that can physically be stacked atop this block
	(meaning its first two dimensions are each smaller than this blocks first
	two dimensions), we append the corresponding indices to a temporary tracker
	that organizes valid "parent" blocks. Amongst the parent blocks, we then
	choose the optimal, that which after its assignment as a base has the
	highest height value. That is then the parent for this block, and the
	optimal height for a tower with this block as its base is equal to that
	parent's corresponding height plus this block's height.

	5. Once this process has been completed for all blocks, our optimal tower is
	found by finding the block that has the highest height value when used as a
	base. The whole stack then is found by tracing the path back through the
	parent array.

	6. We determine the maximum height possible for a constructed stack of
	blocks by returning the max value in the height array.

#### Argue its Correctness

	This algorithm begins by generating all possible orientations of all
	possible blocks. It then proceeds to order these. The advantage of this
	ordering is it makes obvious that no block appearing later than an earlier
	block could possibly have been stacked atop the earlier block. As a result,
	we check later blocks only to see if they could be added below blocks
	that precede them, and never otherwise.

	The advantage of such a method is that each block, when it's considered, can
	search for what stack it can possibly belong to (including a stack of only
	itself) in order to optimize the height of the stack that includes it. This
	is done by finding the tallest substack that can fit atop it so that the
	optimal stack that includes this block as its base has height equal to this
	block plus the height of the tallest substack.

	By the end, we can simply choose amongst all substacks, picking the one with
	the maximum height.

#### Argue its Expected Running Time

	To generate and (merge)sort all orientations of all blocks takes something
	like

		\[
		Theta(6b log (6b)) = Theta(b log b)
			\mbox{ where b is the number of blocks}
		\]

	For each a block at index i, we do i work to find its optimal substack.
	The total work then for b blocks then is

		\[
		\Sum_{i=0}^{6b} i^2 \mbox{ which is O(b^2)}
		\]

	These are the main pieces of work in our alg, and O(b^2) is clearly bigger
	than O(b log b), so we conclude our alg to be at worst O(b^2).

#### "Interesting" Design Decisions

	We initially thought to construct a 2D matrix with the two axes corresponding
	to dimension one and dimension two of the oriented blocks. Then, we used the
	same algorithm as described above to divine the stack of blocks with maximum
	height, however, the barrier to implementing the algorithm with this sort of DP
	structure means that iterating through this matrix could be prohibitively
	expensive in a case where we have blockA: (1,1,1) and blockB
	(10^9,10^9, 1), because the DP matrix constructed would have
	dimensions 10^9 * 10^9.

#### Code Overview Explanation

	We think this has been adequately covered by Describe the Algorithm and
	Argue its Correctness and the extensive comments throughout the file.

#### Testing

	We tested on the provided input files and received identical outputs.

## Acknowledgments

	* Dan Weinand <3





