\documentclass[11pt]{article}

% page setup and prettification
%\usepackage[left=1.5in, right=1.5in]{geometry}
\usepackage{fancyhdr, parskip}
\pagestyle{fancy}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{lmodern, microtype, siunitx}

% important packages
\usepackage[svgnames,x11names]{xcolor}
\usepackage{amsmath, amsthm, amssymb, mathtools, booktabs, commath, dsfont,
setspace, todonotes, xfrac, hyperref, framed, siunitx}
\usetikzlibrary{arrows}
\usepackage[shortlabels]{enumitem}

% commands
\newcommand\todoin[2][]{\todo[inline, caption={2do}, #1]{ %inline todo box
  \begin{minipage}{\textwidth-4pt}#2\end{minipage}}}
\newcommand{\tinytodo}[2][size=\footnotesize]{ %smaller todo
  \todo[caption={#2}, #1]{\begin{spacing}{0.5}#2\end{spacing}}}

% definitions
\newcommand{\problem}[2][]{\begin{framed} \textbf{#1.} #2 \end{framed}}
\newcommand{\indc}[2][-1]{\ensuremath{\mathds{1}\del[#1]{#2}}}
\renewcommand{\exp}{\operatorname{exp}\del}
\renewcommand{\Pr}{\operatorname{Pr}\del}
\newcommand{\E}{\operatorname{E}\del}
\newcommand{\given}[1][]{\:#1\vert\:}
\DeclarePairedDelimiter\ceil{\lceil}{\rceil}
\DeclarePairedDelimiter\floor{\lfloor}{\rfloor}

% header
\lhead{CS 140}
\chead{Assignment 1}
\rhead{Daniel Metz \& Ziqi Xiong}

\begin{document}

\problem[1]{Properties of Logs

  Give proofs for each of the following properties of logarithms. You should
  assume that $a, b, c, n$ are positive \emph{real numbers} (not necessarily
  integers).

  \begin{enumerate}[(a)]
  \item $\log_b a^n = n \log_b a$
  \item $\log_b a = \frac{\log_c a}{\log_c b}$
  \item $a^{\log_{b} n} = n^{\log_{b} a}$
  \end{enumerate}
}

\begin{enumerate}[(a)]
\item
  \begin{proof}
  Set $k = \log_b a^n$, $m = \log_b a$. Then $b^k = a^n$ and $b^m = a$. Thus
  $\del{a}^n = \del{b^m}^n = b^{n \cdot m}$. Therefore $k = n \cdot m$ and so $
  \log_b a^n = n \log_b a$.
  \end{proof}
\item
  \begin{proof}
  Set $k = \log_c a$, $\ell = \log_c b$, $m = \log_b a$. It follows that $c^k =
  a$, $c^\ell = b$, and $b^m = a$. Further, $b^{k/\ell} = \del{c^\ell}^{k/\ell}
  = c^k = a = b^m.$ Thus, $\frac{k}{\ell} = m$ and so $\log_b a = \frac{\log_c
  a}{\log_c b}$.
  \end{proof}
\item
  \begin{proof}
  Set $k = \log_b n$, $m = \log_b a$. Then $b^k = n$, $b^m = a$. It follows then
  that $a^k = \del{b^m}^k = b^{m \cdot k} = \del{b^k}^m = n^m$. Thus $a^{\log_b
  n} = n^{\log_b a}$.
  \end{proof}
\end{enumerate}

\newpage
\problem[2]{The Geometric Series
  \begin{enumerate}[(a)]
  \item Use induction on $n$ to show that for all integers $n \geq 0$
   \[
     1 + a + a^{2} + a^{3} + \ldots + a^{n} = \frac{a^{n+1} - 1}{a - 1}
   \]
  where $a$ is some arbitrary real number other than 1. Please make sure to
  write your proof carefully, with a base case, induction hypothesis, and
  induction step.
  \item Explain where your induction proof relied on the fact that $a \neq 1$.
  \item What does the sum evaluate to when $a = 1$?
  \end{enumerate}
}

\begin{enumerate}[(a)]
\item
  Note that asking to show $1 + a + a^2 + \cdots + a^n = \frac{a^n - 1}{a - 1}$
  is equivalent to asking to show $\sum_{k = 0}^{n} a^k = \frac{a^n - 1}{a - 1}$.

  \begin{proof}
  Let $a \in \mathbb{R}$ such that $a \neq 1$. \\
  \emph{Base case:} ($n = 0$)
    $\sum_{k = 0}^{n} a^k = a^0 = 1 = \frac{a - 1}{a - 1}$. \\
  \emph{Inductive step:}
    Suppose $\sum_{k = 0}^{n} a^k = \frac{a^{n + 1} - 1}{a - 1}$ for some $n \in
    \mathbb{N}$. $\sum_{k = 0}^{n + 1} a^k = \frac{a^{n + 1} - 1}{a - 1} + a^{n
    + 1} =  \frac{a^{n + 1} - 1 + \del{a - 1} a^{n + 1}}{a - 1} =  \frac{a^{n +
    1} - 1 + a^{n + 2} - a^{n + 1}}{a - 1} = \frac{a^{n + 2} - 1}{a - 1}.$ \\
  Thus $\sum_{k = 0}^{n} a^k = \frac{a^{n + 1} - 1}{a - 1}$.
  \end{proof}
\item
  Given that we divide by $a - 1$ on the right side, if $a = 1$ we are
  inappropriately dividing by zero. Thus, we must assume $a \neq 1$ in order
  to permit all of our right-side work.
\item
  When $a = 1$, $\sum_{k = 0}^{n} = 1 + a + a^2 + \cdots + a^n = 1 + 1 + \cdots
  + 1 = n + 1$.
\end{enumerate}

\newpage
\problem[3]{Running Times

  Suppose you have algorithms that execute the following number of operations as
  a function of the input size $n$. If you have a computer that can perform
  $10^{10}$ operations per second, for each algorithm what is the largest input
  size $n$ for which you would be able to get the result within a minute? (For
  once, no explanation necessary!)

  \begin{enumerate}[(a)]
  \item $250n^2$
  \item $n^3$
  \item $\sqrt{n}$
  \item $n\log_2 n$
  \item $2^n$
  \end{enumerate}
}

For each problem, the solution we're looking for is the floor of the answer when
solving: $\textit{expression} = 60 \cdot 10^{10}$.

\begin{enumerate}[(a)]
\item $n = \num{48989}$
\item $n = \num{8434}$
\item $n = \num{3.6e23}$
\item $n = \num{17627806205}$
\item $n = 39$
\end{enumerate}


\newpage
\problem[4]{Asymptotics

  Indicate whether each of the following statements
  is true or false and then carefully \emph{prove} your answer using the formal
  definition of Big-O notation and properties of logarithms from problem 1.
  Remember that to show that one of these statements is false, you must
  obtain a formal contradiction; it does not suffice to just say ``false''.

  \begin{enumerate}[(a)]
  \item $2^{n+1} \mbox{ is } O(2^{n})$.
  \item $2^{2n} \mbox{ is } O(2^{n})$.
  \item $3 n^{2} \log_{2} n + 16 n \mbox{ is } O(n^{3})$.
  \item $25 \log_{2} 8n^{10} \mbox{ is }  O(\log_{10} n)$.
  \item $4^{\log_{2} n} \mbox{ is } O(n^{3})$.
  \end{enumerate}
}

\begin{enumerate}[(a)]
\item
  \begin{proof}
  $2^{n + 1} = 2 \cdot 2^n \leq 3 \cdot 2^n\ \forall n > 0$ thus there exist
  positive constants $c, n_0$ such that $2^{n +1} \leq c \cdot 2^n$ for all $n >
  n_0$.
  \end{proof}
\item
  \begin{proof} \emph{(by contradiction)}
  Suppose $2^{2n}$ is $O(2^n)$. Then $\exists c > 0, n_0 > 0$ such that $\forall
  n > n_0$, $2^{2n} \leq c \cdot 2^n$. It follows then that $\forall n > n_0$,
  $c \cdot 2^n \geq 2^{2n} = \del{2^n}^2$, and so $c \geq 2^n$. Let $n =
  \ceil{\max \del{c, n_0}}.$ For all $k > 0, 2^k > k$. As a result, $c \not \geq
  2^n$ since $2^n > 2^c > c$.  Therefore there does indeed exist some $n > n_0$
  for which $2^{2n} \not \leq c \cdot 2^n $ and so we have a contradiction.
  Therefore, $2^{2n}$ is not $O(2^n)$.
  \end{proof}
\item
  \begin{proof}
  Note that for $n > 0$, $n > \log_2 n$. Similarly, $n^3 > n$ for $n > 1$. As a
  result, $3 n^2 \log_2 n + 16 n \leq 3 n^3 + 16 n$ for $n > 0$. In turn,
  $3 n^3 + 16 n \leq 3 n^3 + 16 n^3 = 19 n^3$ for all $n > 1$. Lastly,
  $19 n^3 < 20 n^3$ for all $n > 1$. Thus there exist postiive constants $c, n_0$
  such that $3 n^2 \log_2 n + 16 n \leq c \cdot n^3$.
  \end{proof}
\item
  \begin{proof}
  Note that
  $25 \log_2 8n^{10}
  = 250 \log_2 8^{\sfrac{1}{10}} n
  = 250 \del{\log_2 8^{\sfrac{1}{10}} + \log_2 n}
  = 250 \del{\frac{\log_{10} 8^{\sfrac{1}{10}}}{\log_{10} 2} + \frac{\log_{10} n}{\log_{10} 2}}
  \leq 250 + 1000 \log_{10} n
  \leq 1001 \log_{10} n$ for all $n > n_0 = \num{10e250}$.
  Thus there exist positive constants $c, n_0$ such that
  $25 \log_2 8n^{10} \leq c \cdot \log_10 n$ for all $n > n_0$.
  \end{proof}
\item
  \begin{proof}
  Note that $4^{\log_2 n} = \del{2^2}^{\log_2 n} = \del{2^{\log_2 n}}^2 = n^2 \leq n^3$
  for all $n > 1$. Thus there exist positive constants $c, n_0$ such that
  $4^{\log_2 n} \leq c \cdot n^3$ for all $n > n_0$.
  \end{proof}
\end{enumerate}

\newpage
\problem[5]{Ranking Functions

  List the functions below in increasing order by placing one function on each
  line, with the top line containing the smallest function and the bottom line
  containing the largest function.If two functions are in the same group
  (functions $f(n)$ and $g(n)$ are in the same group if $f(n) \mbox{ is }
  O(g(n))$ and $g(n) \mbox{ is } O(f(n))$ then write those two functions
  together on the same line. (You should assume, that the size of the problem,
  $n$, is an integer in all cases.) No proofs are necessary, just the correct
  ranking.To help you establish the ranking, you will need to compare pairs of
  functions and you can use any methods that you want. Some useful methods are
  algebraic manipulation (e.g. rewriting a function in another form that allows
  you to compare it more easily with a second function), l'H\^{o}pital's rule to
  compute the limit of the ratio of pairs of functions, and numerical
  experiments (e.g. choose $n$ to be some large power of 2 and then evaluate the
  two functions on that value).

  \begin{center}
  \begin{tabular}{llll}
  $n!$ & $e^n$ & $n\log_4 n$ & $47$ \\
  $n^{1/3} + \log_{5} n$ & $n$ & $(\frac{3}{2})^n$ & $n2^n$\\
  $(\log_2 n)^{\log_2 n}$ & $\sqrt{n}$ & $2^n$ & $n^{\log_2\log_2 n}$ \\
  $\log_2(n!)$ & $n^2$ & $(n+1)!$ & $4^{\log_2 n}$
  \end{tabular}
  \end{center}
}

\begin{table}[h!] \centering
  \caption{Functions, Ranked in Increasing Order}
  \begin{tabular}{l} \toprule
  $47$ \\
  $n^{\sfrac{1}{3}} + \log_5 n$ \\
  $\sqrt{n}$ \\
  $n$ \\
  $\log_2 \del{n!}$ \\
  $n \log_4 n$ \\
  $n^2$, \quad $4^{\log_2 n}$ \\
  $n^{\log_2 \log_2 n}$, \quad $\del{\log_2 n}^{\log_2 n}$\\
  $\del{\frac{3}{2}}^n$ \\
  $2^n $ \\
  $n 2^n$ \\
  $e^n$ \\
  $n!$ \\
  $(n + 1)!$ \\ \bottomrule
  \end{tabular}
\end{table}

\newpage
\problem[6]{The Flash Psychic

The objective of this problem is to reinforce clear and precise writing on
mathematical material.

Take a look at the ``Flash Mind Reader'' at:

\url{http://www.cs.pomona.edu/classes/cs140/ProblemSets/psychic.swf}

Write a clear and precise explanation of how this works.  That is, explain
the mathematics behind this trick.
}

Let us represent a two digit number as $ab$ (not to mean $a \cdot b$). Note that
$ab = 10 a + b$. The website asks us to calculate the value that is
$ab - (a + b)$. Substituting in our calculation of $ab$, we find the website
is expecting a value of $10 a + b - a - b = 9a$. Thus, so long as it predicts
the symbol that corresponds with a multiple of 9 (which every multiple of 9
does indeed share the same symbol), it will be guaranteed to be correct.

\newpage
\problem[7]{The Arithmetic and Geometric Mean

  Let $x_{1}, \ldots, x_{n}$ be positive real numbers.  The \emph{arithmetic
  mean} of these numbers is defined to be $\frac{x_{1} + x_{2} + \ldots +
  x_{n}}{n}$ and the \emph{geometric mean} is defined to be $(x_{1}x_{2}
  \cdots x_{n})^{1/n}$.

	In this problem we show that the arithmetic mean of $n$ numbers is at least
	as large as the geometric mean of those numbers.

	\begin{enumerate}[(a)]
	\item Use induction to show that if $x_{1} x_{2} \cdots x_{n} = 1$
	  then $x_{1} + x_{2} + \ldots + x_{n} \geq n$.
	  Observe that this is a special case of the statement we are trying
	  to prove.

	\item Use this fact to show that the arithmetic mean is at least
	  as large as the geometric mean.  (No induction required here; just
	  a little algebra.)
	\end{enumerate}
}

\begin{enumerate}[(a)]
  \item
    \begin{proof}
  	\emph{Base case:} ($n = 1$)
    If $x_1=1$, then $x_1 \geq 1$ \\
  	\emph{Inductive step:}
  	Suppose $x_1 x_2 \cdots x_n=1$,  then $x_1+x_2+ \cdots +x_n \geq n$.\\
  	Then $x_{n+1} = \frac 1 {x_1 x_2 \cdots x_n} = \frac{1}{1} = 1 $.\\
  	Therefore, $x_1+x_2+\cdots +x_n + x_{n+1} = (x_1+x_2+\cdots +x_n) + x_{n+1} >= n + x_{n+1} = n+1$\\
  	Thus, we have proved the statement.
  	\end{proof}

  \item
  	\begin{proof}
  		$x_1 + x_2 + \cdots + x_n \geq n$
  		\\$ \rightarrow \frac {x_1 + x_2 + \cdots + x_n}{n} \geq 1$
  		\\$ \rightarrow \frac {x_1 + x_2 + \cdots + x_n}{n} \geq x_1 x_2 \cdots x_n \geq \sqrt{x_1 x_2 \cdots x_n}$
  		\\$ \rightarrow$ arithmetic mean $(x_1, x_2,\cdots, x_n) \geq $ geometric mean $(x_1, x_2,\cdots, x_n)$
    \end{proof}
\end{enumerate}

\end{document}
















