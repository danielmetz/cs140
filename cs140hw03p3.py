#!/usr/bin/python
"""
@title: cs140hw03p3
@authors: Daniel Metz, Natalie Casey

module implements code to find the maximum-length non-decreasing subarray
as specified from an input file

usage:
> python cs140hw03p3.py infile outfile
OR if file is directly executable (appropriate file permissions)
> ./cs140hw03p3.py infile outfile
"""

# for command line argument processing
import sys


def max_crossing(prices, low, mid, high):
    """
    Finds the longest non-decreasing subarray between low and high that must
    cross mid. Helper function to max_subarray

    inputs:
        prices (numeric array-like): array to be searched
        low (int): lowest permitted index for search
        mid (int): mid-point index that must be crossed
        high (int): highest permitted index for search
    returns:
        (int) optimal starting index
        (int) optimal ending index
        (int) length of maximum non-decreasing subarray that crosses mid

    attributions: code heavily inspired by Find-Max-Crossing-Subarray algorithm
        in Cormen textbook
    """

    # iterative expansion from midpoint leftward
    left_runner, best_left_index = mid, mid
    while left_runner >= low:
        # expand so long as sequence remains non-decreasing
        # check first across mid-bridge to ensure crossing is logical
        if prices[left_runner] <= prices[left_runner + 1]:
            best_left_index = left_runner
            left_runner -= 1
        else:
            # case where we're no longer non-decreasing
            break

    # iterative expansion from midpoint rightward
    right_runner, best_right_index = mid + 1, mid + 1
    while right_runner <= high:
        # expand so long as sequence remains non-decreasing
        # check first across mid-bridge to ensure crossing is logical
        if prices[right_runner - 1] <= prices[right_runner]:
            best_right_index = right_runner
            right_runner += 1
        else:
            # case where we're no longer non-decreasing
            break

    return best_left_index, best_right_index, best_right_index-best_left_index+1


def max_subarray(prices, low, high):
    """
    Finds the longest non-decreasing subarray between low and high.

    inputs:
        prices (numeric array-like): array to be searched
        low (int): lowest permitted index for search
        high (int): highest permitted index for search
    returns:
        (int) optimal starting index
        (int) optimal ending index
        (int) length of maximum non-decreasing subarray that crosses mid

    attributions: code heavily inspired by Find-Maximum-Subarray algorithm in
        Cormen textbook
    """
    # base case: no more than one element
    if high <= low:
        return low, high, max(high - low, 0)

    # calculate the mid-point
    mid = (low + high) / 2
    # recurse on left-subarray
    left_low, left_high, left_sum = max_subarray(prices, low, mid)
    # recurse on right-subarray
    right_low, right_high, right_sum = max_subarray(prices, mid + 1, high)
    # do a middle-out search for mid-crossing sequences
    cross_low, cross_high, cross_sum = max_crossing(prices, low, mid, high)

    # collect together results
    lows = left_low, right_low, cross_low
    highs = left_high, right_high, cross_high
    sums = left_sum, right_sum, cross_sum

    # find the best result
    choice = sums.index(max(sums))

    # return relevant details describing the best result
    return lows[choice], highs[choice], sums[choice]


def format_output(infile, outfile):
    """
    reads input from infile and write output into outfile

    inputs:
        infile (str): filename or filepath to target input file
        outfile (str): filename or filepath to target output file
    returns:
        (None)

    example:
        infile contents: '8\n42\n40\n45\n45\n44\n43\n50\n49\n'
        outfile contents: '3\n2\n40\n45\n45\n'
            first line is length of longest non-decreasing sub-sequence
            second line is starting index (1-based) of longest non-decreasing
                sub-sequence
            remaining lines are the values that appear in the sub-sequence
    """
    # read in the data
    with open(infile, 'r') as f:
        indata = [int(x) for x in f.read().split()[1:]]

    # do the hard-work
    low, high, length = max_subarray(indata, 0, len(indata) - 1)

    # write the data
    with open(outfile, 'w') as f:
        # write the subarray length
        f.write(str(length))
        f.write('\n')

        # write the starting index (+1 for 1-indexing)
        f.write(str(1 + low))
        f.write('\n')

        # write the relevant values
        for value in indata[low:high + 1]:
            f.write(str(value))
            f.write('\n')


if __name__ == '__main__':
    # read in our command-line arguments
    try:
        infile = sys.argv[1]
        outfile = sys.argv[2]
    except IndexError:
        print 'insufficient command line args given'
        print 'lacking at least an infile or an outfile'
        sys.exit(0)

    # read in the data and write our output
    format_output(infile, outfile)
