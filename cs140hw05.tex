\documentclass[10pt]{article}

% page setup and prettification
\usepackage[hmargin=1.75in, vmargin=.65in]{geometry}
\usepackage{fancyhdr, parskip}
\pagestyle{fancy}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{lmodern, microtype, siunitx}

% important packages
\usepackage[svgnames,x11names]{xcolor}
\usepackage{amsmath,amsthm,amssymb,mathtools,booktabs,commath,dsfont,
setspace,todonotes,xfrac,hyperref,framed,siunitx,listings,tikz,
tikz-qtree,cleveref,outlines,xspace}
\usetikzlibrary{arrows}
\usepackage[shortlabels]{enumitem}

\lstset{showspaces=false, showstringspaces=false, basicstyle=\ttfamily\footnotesize}


% commands
\newcommand\todoin[2][]{\todo[inline, caption={2do}, #1]{ %inline todo box
  \begin{minipage}{\textwidth-4pt}#2\end{minipage}}}
\newcommand{\tinytodo}[2][size=\footnotesize]{ %smaller todo
  \todo[caption={#2}, #1]{\begin{spacing}{0.5}#2\end{spacing}}}

% definitions
\newcommand{\problem}[2][]{\begin{framed} \textbf{#1} #2 \end{framed}}
\newcommand{\indc}[2][-1]{\ensuremath{\mathds{1}\del[#1]{#2}}}
\renewcommand{\exp}{\operatorname{exp}\del}
\renewcommand{\Pr}{\operatorname{Pr}\del}
\newcommand{\E}{\operatorname{E}\del}
\newcommand{\given}[1][]{\:#1\vert\:}
\DeclarePairedDelimiter\ceil{\lceil}{\rceil}
\DeclarePairedDelimiter\floor{\lfloor}{\rfloor}
\newcommand{\bigtheta}[1]{\ensuremath{\Theta(#1)}}
\newcommand{\bigo}[1]{\ensuremath{\mathcal{O}(#1)}}

% header
\setlength{\headheight}{15pt} % to accomodate 12pt font
\lhead{CS 140}
\chead{Assignment 5}
\rhead{Daniel Metz}

\begin{document}

\problem[1. More Heapsort]{
  Recall that Heapsort uses two functions: \textsc{buildheap} and \textsc
  {heapify}. The \textsc{buildheap} function took the initial array and turned
  it into a heap with $n$ elements.  We only used \textsc{buildheap} once.
  Afterwards, we repeatedly swapped the element at the root of the heap with the
  element at location $n$, decrement the value of $n$, and called
  \textsc{heapify} to restore the heap property for the remaining $n-1$
  elements.

  One day you go to a talk in which the speaker claims to have an
  improved version of heapsort that uses \textsc{buildheap}
  without using the \textsc{heapify} function:

  \begin{itemize}
  \item First you call \textsc{buildheap} to get a good heap.
  \item Then, you swap the element at the root with the element at
    location $n$ and decrement the value of $n$.
  \item Now, rather than calling \textsc{heapify}, you just call
    \textsc{buildheap} all over again on the remaining heap of size $n-1$.
  \item You repeat this process until the array is sorted.
  \end{itemize}

  You remark that while it's clearly a \emph{variant} of heapsort, it's
  not really an improvement. How bad is it?  Explain.
}

\newcommand{\buildheap}{\textsc{buildheap}\xspace}
\newcommand{\heapify}{\textsc{heapify}\xspace}

First, recall that a call to \buildheap takes \bigtheta{n} time while a call to
\heapify takes \bigtheta{\log n} time. In calling \buildheap in place of
\heapify, we're calling a computationally more expensive algorithm at each
iteration. Instead of approximately $n$ calls to \heapify on heaps of lengths
$n-1, n-2, \ldots, 2$, we place approximately $n$ calls to \buildheap on heaps
of these lengths. This component of the work then goes from taking
$\sum_{i=2}^{n-2} \log i \leq \log (n!) \approx n \log n$ to $\sum_{i=2}^{n-2} i
\approx n^2$.

The total work of the algorithm then is a call to \buildheap (\bigtheta{n}),
and then the \bigtheta{n^2} work described in the preceeding paragraph. This
latter term dominates the former, so we're left with a \bigtheta{n^2} algorithm
for heapsort, a clear regression in performance.


\newpage
\problem[2. Linked Lists]{
  For each of the four types of lists in the following table, what is the
  asymptotic worst-case running time for each dynamic-set operation listed?  You
  don't need to give any explanations, but please state any assumptions.

  (We discussed this in lecture, but in case you need review: \textsc{Search} is
  given a key, whereas \textsc{Insert}, \textsc{Delete}, \textsc{Successor}, and
  \textsc{Predecessor} are given pointers to the actual elements; the list
  structure is described in section 10.2 of the book; the operations are
  described in the Introduction to Section III.)
}

\newcommand{\btn}{\bigtheta{n}\xspace}
\newcommand{\bto}{\bigtheta{1}\xspace}

\begin{table}[h!] \centering
\begin{tabular}{lcccc} \toprule
& unsorted,  & sorted,  & unsorted, & sorted, \\
& singly linked & singly linked & doubly linked & doubly linked \\ \midrule
\textsc{Search(L,k)}            & \btn & \btn & \btn & \btn \\
\textsc{Insert(L,x)}            & \bto & \ \ \btn$^1$ & \bto & \ \ \btn$^1$ \\
\textsc{Delete(L,x)}            & \ \ \btn$^2$ & \ \ \btn$^2$ & \bto & \bto\\
\textsc{Successor(L,x)}         & \btn & \bto & \btn & \bto \\
\textsc{Predecessor(L,x)}       & \btn & \btn & \btn & \bto \\
\textsc{Minimum(L)}             & \btn & \bto & \btn & \bto \\
\textsc{Maximum(L)}             & \btn & \ \ \ \bto or \btn$^3$ & \btn & \bto \\
\bottomrule
\end{tabular}
\caption{Please note that the 1 and 2 that appear as exponents might are
intended to serve as footnote-style indicators addressed in the enumerated
comments below.}
\end{table}

Central assumption: when given x as an input, x is a pointer to a node.

Further assumptions \& comments:

\begin{enumerate}
\item In the case of \textsc{Insert}, we make a particular point to emphasize
  that x is \emph{not} an iterator pointing to the insertion location within the
  linked list but is a pointer to the node to be inserted. If it were a pointer
  to an iterator already giving the location at which we wish to insert, then
  this reduces to a \bto process.
\item If we're given only a pointer to the particular node we wish to delete, in
  a singly linked list, we'll have to search for the node we wish to delete in
  order to find the node that should preceed it. If however we're given a
  pointer to the node preceeding that node that we wish to delete, then this can
  be reduced to a \bto process.
\item In a sorted singly linked list, if the implementation includes a pointer
  to the final element (its tail), then this may be done in \bto time, else it
  requires a full list traversal, a \btn operation. Note: adding and maintaining
  a tail pointer can be done while adding no more than \bto work to other
  desired operations.
\end{enumerate}


\end{document}
















