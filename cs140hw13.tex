\documentclass[10pt]{article}

% page setup and prettification
\usepackage[hmargin=1in, vmargin=.7in]{geometry}
\usepackage{fancyhdr}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{lmodern,microtype,siunitx,parskip,xspace}

% important packages
\usepackage[shortlabels]{enumitem}
\usepackage{amsmath,amsthm,amssymb,mathtools,booktabs,commath,dsfont,%
  todonotes,xfrac,hyperref,mdframed,cleveref,subcaption,float}
\usepackage{titlesec}

% commands
\newcommand\todoin[2][]{\todo[inline, caption={2do}, #1]{ %inline todo box
  \begin{minipage}{\textwidth-4pt}#2\end{minipage}}}
\newcommand{\tinytodo}[2][size=\footnotesize]{ %smaller todo
  \todo[caption={#2}, #1]{\begin{spacing}{0.5}#2\end{spacing}}}
\newcommand\invisiblesection[1]{%
  \refstepcounter{section}%
  \addcontentsline{toc}{section}{\protect\numberline{\thesection}#1}%
  \sectionmark{#1}}
\newcommand{\problem}[3]{
  \invisiblesection{#1} \begin{mdframed}[] [#2 Points] #3 \end{mdframed}
  }

% definitions
\newcommand{\usd}[1]{\$#1}
\newcommand{\bigtheta}[1]{\ensuremath{\Theta(#1)}}
\newcommand{\bigo}[1]{\ensuremath{\mathcal{O}(#1)}}
\newcommand{\bigomega}[1]{\ensuremath{\Omega(#1)}}

% header
\pagestyle{fancy}
% \setlength{\headheight}{15pt} % to accomodate 12pt font
% \lhead{CS 140 HW 11}
\lhead{CS 140 HW 13 \quad \leftmark}
\rhead{Daniel Metz \& Sean Zhu}
\newcommand{\sectionbreak}{\clearpage} % force problems onto own pages


\begin{document}
\problem{Space Shuttle Profit Optimization}{40}{
  You've been hired by NASA to help optimize their Space Shuttle program.

  On a given mission, NASA will consider a set of experiments that industrial
  sponsors would like to conduct. Let \(E = \cbr{E_1, E_2, \ldots, E_m}\)
  denote the set of experiments under consideration. Let \(p_j\) denote the
  amount of money the sponsor will pay to conduct experiment \(E_j\). The
  experiments use a set \(I = \cbr{I_1, I_2, \ldots, I_n}\) of instruments to
  conduct the experiments. For each experiment \(E_j\), let \(R_j\) be the
  subset of \(I\) that contains all of the instruments needed to conduct
  experiment \(E_j\). Notice that this allows a single instrument to be used in
  multiple experiments. The instruments are potentially large and heavy and the
  cost of taking instrument \(I_k\) is \(c_k\) dollars. Your job is to
  determine which experiments should be performed in order to maximize the net
  revenue, which is the total income from the performed experiments minus the
  total cost of all instruments carried. Amazingly, this problem can be solved
  using \emph{network flow}!

  We'll construct a network flow problem as follows: The network contains a
  source vertex, \(s\), vertices \(I_1, I_2, \ldots, I_n\), vertices \(E_1, E_2,
  \ldots, E_{m}\), and a sink vertex \(t\). For each instrument \(I_k\) there is
  a directed edge from \(s\) to vertex \(I_k\) with capacity \(c_k\) (the cost
  of taking this instrument). For each experiment \(E_j\) there is an edge from
  vertex \(E_j\) to \(t\) with capacity \(p_j\) (the payment for this
  experiment). Finally, if instrument \(I_k\) is in set \(R_j\) (meaning that
  instrument \(I_k\) is needed for experiment \(E_j\)) then there is a directed
  edge from vertex \(I_k\) to vertex \(E_j\) with \emph{infinite} capacity.

  \begin{enumerate}[(a)]
  \item First, try this. Consider a situation in which there are three
    experiments \(E_1, E_2, E_3\) which will bring in \(10\), \(6\), and \(6\)
    dollars, respectively. Also there are four instruments \(I_1\), \(I_2\),
    \(I_3\), and \(I_4\) which cost \(3\), \(2\), \(5\), and \(7\) dollars,
    respectively to take on the shuttle. Experiment \(E_1\) requires instruments
    \(I_1\) and \(I_2\), experiment \(E_2\) requires instruments \(I_1\) and
    \(I_3\), and experiment \(E_3\) requires instruments \(I_3\) and \(I_4\).
    Using brute-force, enumerate all seven possible combinations of experiments
    that could be taken and determine which combination is the most profitable.
    What is this combination and what is the net revenue?

  \item For the example problem above, construct the corresponding network flow
    problem. Find the maximum flow in the network (show your residual graphs at
    each step) and then find the corresponding cut whose capacity is equal to
    this flow.

  \item Let's let \(S\) denote the vertices that are on the same side of the
    above cut as vertex \(s\) and let \(T\) denote the vertices that are on the
    same side of the cut as \(t\). What do you notice about the instruments and
    experiments that are in the set \(T\)?

  \item Now, let \(\tau\) be the sum of the payments that NASA would receive for
    taking all of the possible experiments. In this case, \(\tau = 10 + 6 + 6 =
    22\). From \(\tau\), subtract the capacity of the cut you found above.
    Surprise!  What is this number and how does it appear to relate to this
    problem?

  \item Finally, we're ready to generalize all of this into an efficient
    algorithm for solving the profit maximization problem in general. Assume
    that we've set up a network flow problem corresponding to a given set of
    experiments and instruments. Show that for any cut with finite total
    capacity, if an experiment \(E_j\) is in \(T\) (the side of the cut
    containing vertex \(t\)), then all of the instruments used in this
    experiment must \emph{also} be in \(T\).

  \item Now argue that the maximum net revenue that can be achieved is simply
    the total sum \(\tau\) of the payments that would be received for taking all
    of the experiments minus the capacity of the minimum cut.

  \item Now just summarize all of this by describing the algorithm, step-by-
    step, for finding the maximum net revenue, given a set of experiments,
    instruments, and the corresponding payments and costs. Give a careful
    derivation of the worst-case running time of the algorithm assuming there
    are \(m\) experiments and \(n\) instruments.
  \end{enumerate}
}

\begin{enumerate}[(a)]
\item Looking at \cref{tab:brute-force}, we see that the most profitable
  combination is when we complete experiments \(E_1\) and \(E_2\), which
  generates for us a net revenue of \usd{6}.

  \begin{table}[h] \centering
    \caption{A Brute-Force Evaluation}
    \label{tab:brute-force}
    \begin{tabular}{lrrr} \toprule
    Experiments & Revenue & Cost & Net \\ \midrule
    \(E_1\) & 10 & 5 & 5 \\
    \(E_2\) & 6 & 8 & -2 \\
    \(E_3\) & 6 & 12 & -6 \\
    \(E_1, E_2\) & 16 & 10 & 6 \\
    \(E_1, E_3\) & 16 & 17 & -1 \\
    \(E_2, E_3\) & 12 & 15 & -3 \\
    \(E_1, E_2, E_3\) & 22 & 17 & 5 \\ \bottomrule
    \end{tabular}
  \end{table}
\item
\item Those vertices in set \(S\) are those we've chosen to exclude from our
  undertaking. For an instrument, this means it is an instrument we've chosen
  not to buy (and therefore one whose costs we need not bear). For an
  experiment, this means it is an experiment we've chosen not to complete (and
  therefore one whose revenue we may not collect). Those vertices in set \(T\)
  are those we've chosen to include, whether it means buying the instrument (and
  bearing its cost) or conducting the experiment (and whose resultant revenue
  we collect).
\item This number is our net revenue, which here is 6 (found by \(22 - 16 =
  6\)). We can break this down more intuitively as the total revenue collectable
  from all experiments, minus the costs of the instruments we choose to buy,
  minus the revenue we cannot collect from the experiments that we cannot
  conduct based on our instrument set.
\item Suppose towards contradiction that for some cut of finite total capacity,
  there exists some \(j\) such that experiment \(E_j \in T\) and there exists
  some \(i\) such that \(I_i \in R_j\) and \(I_i \in S\). Since \(I_i \in R_j\),
  there exists an edge of infinite capacity from \(I_i\) to \(E_j\). Recall that
  \(I_i \in S\) and \(E_j \in T\), meaning the capacity of the cut is at least
  the weight of this edge as this edge goes from \(S\) to \(T\). This means that
  the total capacity of the cut is infinite, which contradicts our initial
  premise. Thus we conclude that For any cut with finite total capacity, if an
  experiment \(E_j\) is in \(T\) (the side of the cut containing vertex \(t\)),
  then all of the instruments used in this experiment must \emph{also} be in
  \(T\).
\item The edge capacity between \(s\) and \(I_i\) is the cost of instrument
  \(I_i\). The edge capacity between \(E_j\) and \(t\) is the revenue earned if
  experiment \(E_j\) is conducted.  Given an \(S, T\) cut, if an edge from \(s\)
  to \(I_i\) for some \(i\) crosses from \(S\) to \(T\), the capcity of our cut
  includes the cost of instrument \(I_i\). If an edge from \(E_j\) to
  \(t\) for some \(j\) crosses from \(S\) to \(T\), the capacity of our cut
  includes the would-be revenue generated by conducting experiment \(E_j\). We
  claim that each constituent value that composes part of the capacity of
  our min-cut is either one of these instrument costs or one of these would-be
  revenue costs. We justify this by claim that no other edge can be included, as
  the only other type of edge to be considered would be an edge between some
  instrument and an experiment that depends on this instrument. However, such an
  edge has infinite capacity, and therefore cannot be part of any min-cut.

  Given our definition of \(\tau\) to equal the total possible revenue (the sum
  of all experiment revenues), subtracting away the value of the cut, which is
  composed of revenues we don't earn and instrument costs we choose to bear,
  leaves us with our net revenue. We argue this to be maximal as \(\tau\) is
  fixed and we've made our cut minimal.
\item Consider our algorithm to be as follows. Find the value of the max-flow by
  utilization of the Edmonds-Karp Algorithm. Calculate \(\tau = \sum_{j=1}^{n}
  p_j \). Finally, calculate the maximum net revenue by taking the difference
  between \(\tau\) and the value of the max-flow.

  As for run-time analysis, the textbook claims the worst-case time for Edmonds-
  Karp to be \bigo{VE^2}. However, the network flow that we're considering is a
  special case, and therefore we can push down this bound. The textbook begins
  by noting that each of the \abs{E} edges can go from critical to non-critical
  at most \sfrac{\abs{V}}{2} times. However, there are at most \(n\) experiments
  to which flow can be re-allocated, so for us the number of times each edge can
  go from critical to non-critical is \sfrac{\abs{V}}{n}.

  Again in the textbook, they consider that ther eare \bigo{E} pairs of vertices
  that can have an edge between then in the residual network. For us, while this
  is true definitionally, we can be more specific. Our number of edges will be
  \bigo{mn} as our graph will have \bigo{m} edges connecting \(s\) and our
  instruments, \bigo{n} edges connecting our experiments and \(t\), and \bigo%
  {mn} edges in the worst-case connecting our instruments.

  Given the textbook claims the entire execution of the Edmonds-Karp algorithm
  to be the multiplication between the values concluded in each of the two
  preceeding paragraphs, the entire execution for us is then worst-case \bigo%
  {\abs{V} mn} (sadly our numerator and denominator \(n\)s do not cancel due to
  their coefficients.

  The textbook also notes that we need to multiply by the cost of
  Ford-Fulkerson, which they claim to be \bigo{E} time. However for us, our
  Ford-Fulkerson run will have a run-time cost of \bigo{1} since all augmenting
  paths have at most length 4 (from \(s\) to an instrument to an experiment to
  \(t\)).

  This lets us conclude our run-time to be \bigo{\abs{V} mn}.
\end{enumerate}


\end{document}
