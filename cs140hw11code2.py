import numpy as np


INPUT = np.array([
    [  1.,          0.8,         6.66666667],
    [  1.25,        1.,          10.       ],
    [  0.15,        0.1,         1.        ]
])

OUTPUT = np.array([
     [  1.2,   0.8,   8.  ],
     [  1.5,   1.2,   10. ],
     [  0.15,  0.12,  1.2 ]
])

def check_and_convert_adjacency_matrix(adjacency_matrix):
    mat = np.asarray(adjacency_matrix)

    (nrows, ncols) = mat.shape
    assert nrows == ncols
    n = nrows

    # assert (np.diagonal(mat) == 0.0).all()

    return (mat, n)

def floyd_warshall_cormen(adjacency_matrix, D):
    '''An exact implementation of the Floyd-Warshall algorithm as described in
    Cormen, Leiserson and Rivest.'''
    (mat, n) = check_and_convert_adjacency_matrix(adjacency_matrix)

    matrix_list = [mat]
    for k in xrange(n):
        next_mat = np.zeros((n,n))
        for i in xrange(n):
            for j in xrange(n):
                next_mat[i,j] = max(mat[i,j], mat[i,k] * D[k,j])
        mat = next_mat
        matrix_list.append(mat)

    return matrix_list[-1]

if __name__ == '__main__':
    print floyd_warshall_cormen(INPUT, INPUT)

    print OUTPUT