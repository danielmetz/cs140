def skip_generation(tree):
    """
    return the collection of trees that are grandchildren of the root
    """
    # handle the case where our tree is empty (rootless)
    # and the case where there is a root but it doesn't have children
    if not tree.root or not tree.root.children:
        return []

    # grab the root node
    root = tree.root

    # initialize an array
    grandchildren = []

    # iterate through each child of the root
    for child in root.children:
        # and for each of the children of that child
        for grandchild in child.children:
            # add them to the array
            grandchildren.append(grandchild)

    return grandchildren

def max_fun(tree):
    """
    maimize the total sum of fun_coef given that a parent and child
    (exactly 1 generation apart) may not both be included
    """
    # handle the case in which our tree is empty
    if not tree.root:
        return 0

    # use it case
    skipped_generation = skip_generation(tree)
    use_it = tree.root.fun_coef + \
        sum(max_fun(subtree) for subtree in skipped_generation)

    # lose it case
    lose_it = sum(max_fun(child) for child in tree.root.children)

    return max(use_it, lose_it)