def optimal_assignment(skiers, skis, total_disp=0):
	# if we have only one reasonable assignment
	if len(skiers) == len(skis) or not skiers:
		return sum(abs(skiers[i] - skis[i]) for i in xrange(len(skiers))) + \
			total_disp

	# we can choose to assign the smallest pair of skis to the shortest person
	short_assignment = optimal_assignment(skiers[1:], skis[1:],
		total_disp + abs(skiers[0] - skis[0]))

	# or we choose not to
	not_assignment = optimal_assignment(skiers, skis[1:], total_disp)

	# choose the smarter of the two possible paths
	return min(short_assignment, not_assignment)


if __name__ == '__main__':
	skiers = [3, 5, 7]
	skis = [2, 3, 4, 5, 9]
	print optimal_assignment(skiers, skis)
