\documentclass[10pt]{article}

% page setup and prettification
\usepackage[hmargin=1.75in, vmargin=.65in]{geometry}
\usepackage{fancyhdr, parskip}
\pagestyle{fancy}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{lmodern, microtype, siunitx}

% important packages
\usepackage[svgnames,x11names]{xcolor}
\usepackage{amsmath, amsthm, amssymb, mathtools, booktabs, commath, dsfont,
setspace, todonotes, xfrac, hyperref, framed, siunitx, listings, tikz,
tikz-qtree, cleveref, outlines}
\usetikzlibrary{arrows}
\usepackage[shortlabels]{enumitem}

\lstset{showspaces=false, showstringspaces=false, basicstyle=\ttfamily\footnotesize}


% commands
\newcommand\todoin[2][]{\todo[inline, caption={2do}, #1]{ %inline todo box
  \begin{minipage}{\textwidth-4pt}#2\end{minipage}}}
\newcommand{\tinytodo}[2][size=\footnotesize]{ %smaller todo
  \todo[caption={#2}, #1]{\begin{spacing}{0.5}#2\end{spacing}}}

% definitions
\newcommand{\problem}[2][]{\begin{framed} \textbf{#1} #2 \end{framed}}
\newcommand{\indc}[2][-1]{\ensuremath{\mathds{1}\del[#1]{#2}}}
\renewcommand{\exp}{\operatorname{exp}\del}
\renewcommand{\Pr}{\operatorname{Pr}\del}
\newcommand{\E}{\operatorname{E}\del}
\newcommand{\given}[1][]{\:#1\vert\:}
\DeclarePairedDelimiter\ceil{\lceil}{\rceil}
\DeclarePairedDelimiter\floor{\lfloor}{\rfloor}

% header
\lhead{CS 140}
\chead{Assignment 4}
\rhead{Daniel Metz \& Natalie Casey}

\begin{document}

\problem[1. Sorting Partially Sorted Data]{
  \begin{enumerate}[(a)]
  \item You are given an array of $n$ elements to sort.  The good news is that
  the array is already partitioned into $n/k$ blocks of $k$ elements each.  The
  elements in the first block (elements at array indices $1$ through $k$) are
  unsorted, but they are \emph{all} less than the elements in the second block
  (elements at array indices $k+1$ through $2k$), and so forth.  In other words,
  each of the $n/k$ blocks is unsorted, but the elements in each block are
  strictly smaller than the elements in the next block.

  Prove that any comparison-based sorting algorithm that receives this kind of
  ``partially'' sorted data has a lower bound of $\Omega(n \log k)$ on its
  worst-case running time.

  \emph{Note:} It is not at all rigorous nor correct to simply combine the
  $\Omega(k \log k)$ lower bounds for sorting each of the $n/k$ blocks!  To see
  why, observe that a skeptic could rightfully ask if there might not be a
  special clever algorithm that exploits the information about the blocks to do
  better than we would without knowing that information.  A rigorous proof will
  need to follow the paradigm that we used in class to get the lower bound for
  sorting in general.

  \item Briefly describe how such an array (an array of length $n$
    with $n/k$ subsequences such that the elements in each subsequence are all
    smaller than the elements in the next subsequence) can be sorted in time
    $O(n \log k)$.  From the first part of this problem, you can now conclude
    that your algorithm is asymptotically optimal!
  \end{enumerate}
}

\begin{enumerate}[(a)]
\item We begin by considering the number of ways that the elements in an array
  as described may be permuted. Each block of $k$ elements has $k!$
  permutations. We know that these blocks are already in order, and therefore we
  need not consider the permutations of block arrangements. However, we note
  that given each block may be independently permuted, the number of array
  permutations possible is then equal to $k!^{\sfrac{n}{k}}$. Comparison based
  sort splits the problem in two, therefore $\log k!^{\sfrac{n}{k}}$ is the
  shortest height attainable by a binary decision tree with $k!^{\sfrac{n}{k}}$
  leaves.

  From here, we note that $\log k!^{\sfrac{n}{k}} = \frac{n}{k} \log k!$. Recall
  that $\log k!$ is $\Theta(k \log k)$ so then $\sfrac{n}{k} \cdot k \log k = n
  \log k$ we use to conclude that $\log k!^ {\sfrac {n} {k}}$ is $\Theta(n \log
  k)$.

\item For each block of size $k$, use Mergesort ($\Theta(k \log k)$) to sort the
  elements of the block. Because there are $\sfrac{n}{k}$ blocks, in the worst-
  case we've just done $k \log k$ work $\sfrac{n}{k}$ times, giving a total
  run-time of $\mathcal {O} (n \log k)$. This matches our lower bound found in
  (a) and so we may conclude our algorithm to be asymptotically optimal.

\end{enumerate}


\newpage
\problem[2. The Index Problem]{
  Let $A$ be an array of $n$ distinct integers where $A$ is already
  \textbf{sorted} in ascending order. Our problem is to find an index $i, 1 \leq
  i \leq n$, such that $A[i] = i$ or determine that no such $i$ exists.
  \begin{enumerate}[(a)]
  \item Describe an algorithm for this problem with  $O(\log n)$ running time.
    You should give the algorithm (in clear English or in clear and commented
    high-level pseudo-code) and briefly explain why the running is $O(\log n)$
    in the worst case.
  \item Show that any comparison-based algorithm for this problem must have
    $\Omega(\log n)$ worst-case running time. (\emph{Note:} Use an argument very
    similar to the $\Omega(n \log n)$ lower bound we achieved for comparison-%
    based sorting.)  From this you can conclude that you found an asymptotically
    optimal algorithm for this problem in part (a).
  \end{enumerate}
}

\begin{enumerate}[(a)]
\item
  \lstinputlisting[language=Python]{cs140hw04p2.py}

  The algorithm described above splits the problem size in half at each step. In
  the worst-case, we must continue splitting the problem size until a base-case
  is reached (subarray under consideration is of length one). Given one can only
  split an array of length $n$ in half $\log_2 n$ times before arriving at an
  subarray of length one, we conclude that our run-time is $\mathcal{O}(\log n)$.

\item
  Consider that for an array of length $n$, there are $n$ possible indices,
  $i$, for which it's possible that $i = A[i]$. At every step of a comparison
  based algorithm for this problem, we split the array in two parts, and recurse
  only on the relevant part. Thinking of a decision tree, we know we must end
  with $n$ leaves, each representing a possible solution. We can see that in the
  best case, these leaves are all on the same level. Combining this information
  with the fact that each comparison splits the problem in two, we conclude that
  an optimal tree can have a maximum height no larger than $\log_2 n$. Thus this
  Given that a step from one level of the tree down to the next level of the
  tree requires a single comparison, in the worst-case, an optimal algorithm
  must make $\log_2 n$ comparisons, giving a lower bound run-time of $\Omega
  (\log n)$.
\end{enumerate}

\newpage
\problem[3. Order Statistics Revisited]{
  In class we showed that the recursive \textbf{Select} algorithm runs in
  worst-case time $\Theta(n \lg n)$ if the array is partitioned into groups of
  3 but runs in time $\Theta(n)$ if the array is partitioned into groups of 5.
  Redo the analysis with groups of $7$: give the recurrence relation, explain
  why it is correct, then derive the Big-Theta running time using the guess-and-
  check method from class.
}

We claim $T(n) = T \del{\sfrac{n}{7}} + T \del{\sfrac{5n}{7}} + c n$ and $T (1)
= d$. We argue this to be the case as a recursive call of Select must be done on
all of the medians selected after the first iteration (of which there are
\sfrac{n}{7} given there is one per group and there are \sfrac{n}{7} groups). A
recursive call must also be done on either the elements smaller than our median
of medians or those elements that are larger. For worst-case consideration, we
assume recursion on the larger of the choices. The number of elements in the
larger selection, is equal $n$ minus the number of elements in the smaller
section. If we organize our array (in our heads) as being a series of columns
comprised of seven rows each, the elements guaranteed to be smaller than our
median of medians can come from either of two disjoint sets. The first set of
elements is comprised of the medians smaller than or equal to the median of
medians, of which we know there to be $\sfrac{1}{2} \cdot \sfrac{n}{7} =
\sfrac{n}{14}$. The second set is the three elements smaller than each of the
medians selected in the previous set of which there $3 \cdot \sfrac{n}{14} =
\sfrac{3n}{14}$. Together, these two sets comprise \sfrac{2n}{7} elements. The
larger possible set of elements on which to recurse then must contain
\sfrac{5n}{7} elements. Lastly, there's a constant term to account for the work
of finding the median of each group, which we must do \sfrac{n}{7} times, giving
us a $c n$ term for this part of the process. For the case where $n = 1$,
the amount of work required is trivial as our answer will be merely that single
element. Altogether then, we conclude the recurrence relation to be $T(n) = T
\del{\sfrac{n}{7}} + T \del{\sfrac{5n}{7}} + c n$ with $T(1) = d$.


We now prove the runtime to be $\Theta(n)$.

\begin{proof} (by strong induction)
\emph{IHOP}: Suppose $T(m) \leq k m$ for all $m < n$. \\

\emph{Base Case}: ($n = 1$) $T(n) = T(1) = d$. Our base case satisfies our
inductive hypothesis so long as $k \geq d$.

\emph{Inductive Step}: Let $n > m$. $T(n) = T \del{\sfrac{n}{7}} + T \del
{\sfrac{5n}{7}} + c n$. Since both \sfrac{n}{7} and \sfrac{5n}{7} are less than
$n$, the inductive hypothesis may be applied. Therefore $T(n) \leq \sfrac{kn}
{7} + \sfrac{5kn}{7} + cn = \del{\sfrac{6k}{7} + c} n$. Note we wish to show
$T(n) \leq kn$, motivating us to chose $k$ such that $\sfrac{6k}{7} + c \leq k$.
In other words, $k \geq 7c$.

In order to satisfy the aforementioned constraint and the constraint from the
base case, we choose $k \geq \max (d, 7c)$. Since we may do so, we conclude that
$T(n)$ is $\Theta(n)$.
\end{proof}

\end{document}
















