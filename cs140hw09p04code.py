#!/usr/bin/python

"""
This file implements a solution for CS140 HW09 P04 in Fall 2015

authors: Daniel Metz & Charlie Watson
"""
import sys
from itertools import chain, permutations

def process_input():
    """
    this function should properly parse the input command in the form
    "./blocks <infile.txt> <outfile.txt>"
    for use in the blocks function
    """
    try:
        infile = sys.argv[1]
        outfile = sys.argv[2]
    except:
        print 'An error occurred'
        print 'Make sure your run was in the form of'
        print 'python cs140hw09p04code.py <infile> <outfile>'
        sys.exit(0)

    # read in the block possibilities
    with open(infile) as f:
        blocks = [line.split() for line in f.xreadlines() if line.split()][1:]

    # convert string blocks to int blocks
    for block in blocks:
        block[0] = int(block[0])
        block[1] = int(block[1])
        block[2] = int(block[2])

    # call our solver
    solve(blocks, outfile)

def init(blocks):
    """
    This function merely expands our set of blocks to include each block in each
    of its six possible orientations
    """
    # find all possible orientations of all blocks
    oriented_blocks = tuple(chain(*[permutations(block) for block in blocks]))
    return sorted(oriented_blocks)


def fits_on(top, bot=None):
    """ check if top fits on bot """
    return top[0] < bot[0] and top[1] < bot[1]


def argmax(iterable):
    """ implements an argmax function """
    biggest = max(iterable)
    for index, element in enumerate(iterable):
        if element == biggest:
            return index


def dp_search(oriented_blocks):
    """
    uses dynamic programming to find the optimal stacking of blocks
    """
    # initialize a stack ending with a gien block to have height zero
    heights = [0] * len(oriented_blocks)
    # initialize all stacks to have no parent block (block placed atop this one)
    parent = [None] * len(oriented_blocks)

    # for each block, calculate where it fits/how it affects other stacks
    for index, block in enumerate(oriented_blocks):
        # the indices of blocks that can be stacked atop block
        stack_index=[]
        # the corresponding actual blocks of the above
        stack_block=[]
        # check to see what previous blocks can actually fit atop this block
        for prev_index in xrange(index):
            if fits_on(oriented_blocks[prev_index], block):
                stack_index.append(prev_index)
                stack_block.append(oriented_blocks[prev_index])

        # find the best pre-this-block height that this block permits
        if stack_index:
            heights[index] = max(heights[ind] for ind in stack_index)

        # similarly, if this block will have a block atop it, find which
        for ind in stack_index:
            if heights[ind] == heights[index]:
                parent[index] = ind
                break

        # add the height of this block to our height-tracker
        heights[index] += block[2]

    return heights, parent


def trace(heights, parent, oriented_blocks):
    """
    get the path of the optimal block stack
    """
    # start with the base block that can build up to the tallest tower
    ancestry = [argmax(heights)]
    # keep building until there's no parent left
    while parent[ancestry[-1]] is not None:
        ancestry.append(parent[ancestry[-1]])

    # convert those indices back to the actual blocks
    ordered_blocks = [oriented_blocks[block] for block in ancestry]
    return ordered_blocks


def solve(blocks, outfile):
    """
    our master function
    muhaha

    grabs all possible orientations of all blocks,
    calls our dp_search function, and traces the path to determine the stack

    prints out:
        'The tallest tower has X blocks and a height of Y'

    writes the desired output to outfile
    """
    oriented_blocks = init(blocks)
    heights, parent = dp_search(oriented_blocks)
    path = trace(heights, parent, oriented_blocks)

    print 'The tallest tower has {} blocks and a height of {}'.format(
        len(path), max(heights))

    with open(outfile, 'w') as f:
        # write the number of blocks involved in the tallest tower
        f.write(str(len(path)))
        f.write('\n')

        # write those blocks, Morty!
        for block in path:
            for index, elem in enumerate(block):
                f.write(str(elem))
                if index < 2: # the last bit doesn't need a trailing space
                    f.write(' ')
            f.write('\n')


if __name__ == '__main__':
    """ start the process by processing """
    process_input()


